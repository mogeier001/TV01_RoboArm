<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="100" unitdist="mil" unit="mil" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="ReferenceLS" color="12" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="10" fill="10" visible="no" active="no"/>
<layer number="210" name="210bmp" color="11" fill="10" visible="no" active="no"/>
<layer number="211" name="211bmp" color="12" fill="10" visible="no" active="no"/>
<layer number="212" name="212bmp" color="13" fill="10" visible="no" active="no"/>
<layer number="213" name="213bmp" color="14" fill="10" visible="no" active="no"/>
<layer number="214" name="214bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="215" name="215bmp" color="16" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="17" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="RobticArm">
<packages>
<package name="2POL254" urn="urn:adsk.eagle:footprint:9305/1" locally_modified="yes">
<description>&lt;b&gt;PHOENIX CONNECTOR&lt;/b&gt;</description>
<pad name="1" x="-2.54" y="0" drill="1.5" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.5" shape="long" rot="R90"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<rectangle x1="-5.08" y1="3.81" x2="5.08" y2="5.08" layer="21"/>
</package>
<package name="CAPC2012X135" urn="urn:adsk.eagle:footprint:7158980/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 1.35 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.35 mm&lt;/p&gt;</description>
<wire x1="0.272125" y1="-0.675" x2="-0.272125" y2="-0.675" width="0.12" layer="21"/>
<wire x1="-0.272125" y1="-0.675" x2="-0.272125" y2="0.675" width="0.12" layer="21"/>
<wire x1="-0.272125" y1="0.675" x2="0.272125" y2="0.675" width="0.12" layer="21"/>
<wire x1="0.272125" y1="0.675" x2="0.272125" y2="-0.675" width="0.12" layer="21"/>
<smd name="1" x="-0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<smd name="2" x="0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<text x="0" y="0.896125" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.896125" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.275271875" y1="-0.642303125" x2="0.275271875" y2="0.642303125" layer="21"/>
</package>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:7130524/1" locally_modified="yes">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.90 X 3.90 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.90 X 3.90 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7396" y="2.744" radius="0.25" width="0" layer="21"/>
<smd name="1" x="-2.4805" y="1.905" dx="1.9972" dy="0.67" layer="1"/>
<smd name="2" x="-2.4805" y="0.635" dx="1.9972" dy="0.67" layer="1"/>
<smd name="3" x="-2.4805" y="-0.635" dx="1.9972" dy="0.67" layer="1"/>
<smd name="4" x="-2.4805" y="-1.905" dx="1.9972" dy="0.67" layer="1"/>
<smd name="5" x="2.4805" y="-1.905" dx="1.9972" dy="0.67" layer="1"/>
<smd name="6" x="2.4805" y="-0.635" dx="1.9972" dy="0.67" layer="1"/>
<smd name="7" x="2.4805" y="0.635" dx="1.9972" dy="0.67" layer="1"/>
<smd name="8" x="2.4805" y="1.905" dx="1.9972" dy="0.67" layer="1"/>
<text x="0" y="3.629" size="0.8128" layer="25" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.189" size="0.8128" layer="27" font="vector" align="top-center">&gt;VALUE</text>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
</package>
<package name="LEDC2012X70" urn="urn:adsk.eagle:footprint:7159536/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<smd name="C" x="-0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<smd name="A" x="0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<text x="0" y="0.896125" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.896125" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="0.275271875" y="0.734059375"/>
<vertex x="-0.275271875" y="0.09175625"/>
<vertex x="-0.275271875" y="0"/>
<vertex x="0.275271875" y="-0.734059375"/>
</polygon>
</package>
<package name="RESC2016X70" urn="urn:adsk.eagle:footprint:7158856/1" locally_modified="yes">
<description>Chip, 2.00 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<wire x1="0.272125" y1="-0.85" x2="-0.272125" y2="-0.85" width="0.127" layer="21"/>
<wire x1="-0.272125" y1="-0.85" x2="-0.272125" y2="0.85" width="0.127" layer="21"/>
<wire x1="-0.272125" y1="0.85" x2="0.272125" y2="0.85" width="0.127" layer="21"/>
<wire x1="0.272125" y1="0.85" x2="0.272125" y2="-0.85" width="0.127" layer="21"/>
<smd name="1" x="-0.9399" y="0" dx="1.0202" dy="1.8" layer="1"/>
<smd name="2" x="0.9399" y="0" dx="1.0202" dy="1.8" layer="1"/>
<text x="0" y="1.071125" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.071125" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM4226X243" urn="urn:adsk.eagle:footprint:7189003/1" locally_modified="yes">
<description>Molded Body, 4.29 X 2.69 X 2.44 mm body
&lt;p&gt;Molded Body package with body size 4.29 X 2.69 X 2.44 mm&lt;/p&gt;</description>
<smd name="C" x="-1.6761" y="0" dx="2.2846" dy="1.5839" layer="1"/>
<smd name="A" x="1.6777" y="0" dx="2.2814" dy="1.5871" layer="1"/>
<text x="0" y="1.1305" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1305" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-0.36703125" y="0"/>
<vertex x="0.367028125" y="0.734059375"/>
<vertex x="0.36703125" y="0.734059375"/>
<vertex x="0.36703125" y="-0.734059375"/>
<vertex x="-0.367028125" y="0"/>
</polygon>
</package>
<package name="HDRVR40W64P254_2X20_5080X508X254B" urn="urn:adsk.eagle:footprint:7166139/1" locally_modified="yes">
<description>Double-row, 40-pin Receptacle Header (Female) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 2.54 mm insulator length, 50.80 X 5.08 X 2.54 mm body
&lt;p&gt;Double-row (2X20), 40-pin Receptacle Header (Female) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 2.54 mm insulator length with overall size 50.80 X 5.08 X 2.54 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<circle x="-24.13" y="3.044" radius="0.25" width="0" layer="22"/>
<wire x1="25.4" y1="2.54" x2="-25.4" y2="2.54" width="0.12" layer="22"/>
<wire x1="-25.4" y1="2.54" x2="-25.4" y2="-2.54" width="0.12" layer="22"/>
<wire x1="-25.4" y1="-2.54" x2="25.4" y2="-2.54" width="0.12" layer="22"/>
<wire x1="25.4" y1="-2.54" x2="25.4" y2="2.54" width="0.12" layer="22"/>
<wire x1="25.4" y1="2.54" x2="-25.4" y2="2.54" width="0.12" layer="52"/>
<wire x1="-25.4" y1="2.54" x2="-25.4" y2="-2.54" width="0.12" layer="52"/>
<wire x1="-25.4" y1="-2.54" x2="25.4" y2="-2.54" width="0.12" layer="52"/>
<wire x1="25.4" y1="-2.54" x2="25.4" y2="2.54" width="0.12" layer="52"/>
<pad name="1" x="-24.13" y="1.27" drill="1.1051" diameter="1.7051" shape="square"/>
<pad name="2" x="-24.13" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="-21.59" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="-21.59" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="5" x="-19.05" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="6" x="-19.05" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="7" x="-16.51" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="8" x="-16.51" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="9" x="-13.97" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="10" x="-13.97" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="11" x="-11.43" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="12" x="-11.43" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="13" x="-8.89" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="14" x="-8.89" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="15" x="-6.35" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="16" x="-6.35" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="17" x="-3.81" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="18" x="-3.81" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="19" x="-1.27" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="20" x="-1.27" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="21" x="1.27" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="22" x="1.27" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="23" x="3.81" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="24" x="3.81" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="25" x="6.35" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="26" x="6.35" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="27" x="8.89" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="28" x="8.89" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="29" x="11.43" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="30" x="11.43" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="31" x="13.97" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="32" x="13.97" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="33" x="16.51" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="34" x="16.51" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="35" x="19.05" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="36" x="19.05" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="37" x="21.59" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="38" x="21.59" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="39" x="24.13" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="40" x="24.13" y="-1.27" drill="1.1051" diameter="1.7051"/>
<text x="0" y="3.175" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<pad name="P$1" x="-29" y="0" drill="2.75" diameter="6.2"/>
<pad name="P$2" x="29" y="0" drill="2.75" diameter="6.2"/>
<pad name="P$3" x="29" y="49" drill="2.75" diameter="6.2"/>
<pad name="P$4" x="-29" y="49" drill="2.75" diameter="6.2"/>
<wire x1="27" y1="-4" x2="33" y2="-4" width="0.127" layer="21"/>
<wire x1="33" y1="-4" x2="33" y2="3" width="0.127" layer="21"/>
</package>
<package name="HDRV3W64P254_1X3_762X254X838B" urn="urn:adsk.eagle:footprint:7162117/1" locally_modified="yes">
<description>Single-row, 3-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X3), 3-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.12" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.12" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.12" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<text x="-2.54" y="-2.421" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
</package>
<package name="SOP65P640X110-28" urn="urn:adsk.eagle:footprint:7189290/1" locally_modified="yes">
<description>28-SOP, 0.65 mm pitch, 6.40 mm span, 9.65 X 4.40 X 1.10 mm body
&lt;p&gt;28-pin SOP package with 0.65 mm pitch, 6.40 mm span with body size 9.65 X 4.40 X 1.10 mm&lt;/p&gt;</description>
<circle x="-2.9646" y="4.9786" radius="0.25" width="0" layer="21"/>
<smd name="1" x="-2.8741" y="4.225" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="2" x="-2.8741" y="3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="3" x="-2.8741" y="2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="4" x="-2.8741" y="2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="5" x="-2.8741" y="1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="6" x="-2.8741" y="0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="7" x="-2.8741" y="0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="8" x="-2.8741" y="-0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="9" x="-2.8741" y="-0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="10" x="-2.8741" y="-1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="11" x="-2.8741" y="-2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="12" x="-2.8741" y="-2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="13" x="-2.8741" y="-3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="14" x="-2.8741" y="-4.225" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="15" x="2.8741" y="-4.225" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="16" x="2.8741" y="-3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="17" x="2.8741" y="-2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="18" x="2.8741" y="-2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="19" x="2.8741" y="-1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="20" x="2.8741" y="-0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="21" x="2.8741" y="-0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="22" x="2.8741" y="0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="23" x="2.8741" y="0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="24" x="2.8741" y="1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="25" x="2.8741" y="2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="26" x="2.8741" y="2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="27" x="2.8741" y="3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="28" x="2.8741" y="4.225" dx="1.6101" dy="0.4992" layer="1"/>
<text x="0" y="5.8636" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.535" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-1.92690625" y1="-4.95490625" x2="1.92690625" y2="-4.95490625" width="0.127" layer="21"/>
<wire x1="1.92690625" y1="-4.95490625" x2="1.92690625" y2="4.95490625" width="0.127" layer="21"/>
<wire x1="1.92690625" y1="4.95490625" x2="-1.92690625" y2="4.95490625" width="0.127" layer="21"/>
<wire x1="-1.92690625" y1="4.95490625" x2="-1.92690625" y2="-4.95490625" width="0.127" layer="21"/>
</package>
<package name="SOIC127P600X175-16" urn="urn:adsk.eagle:footprint:7189513/1" locally_modified="yes">
<description>16-SOIC, 1.27 mm pitch, 6.00 mm span, 9.90 X 3.90 X 1.75 mm body
&lt;p&gt;16-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 9.90 X 3.90 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7059" y="5.284" radius="0.25" width="0" layer="21"/>
<smd name="1" x="-2.4796" y="4.445" dx="1.9645" dy="0.67" layer="1"/>
<smd name="2" x="-2.4796" y="3.175" dx="1.9645" dy="0.67" layer="1"/>
<smd name="3" x="-2.4796" y="1.905" dx="1.9645" dy="0.67" layer="1"/>
<smd name="4" x="-2.4796" y="0.635" dx="1.9645" dy="0.67" layer="1"/>
<smd name="5" x="-2.4796" y="-0.635" dx="1.9645" dy="0.67" layer="1"/>
<smd name="6" x="-2.4796" y="-1.905" dx="1.9645" dy="0.67" layer="1"/>
<smd name="7" x="-2.4796" y="-3.175" dx="1.9645" dy="0.67" layer="1"/>
<smd name="8" x="-2.4796" y="-4.445" dx="1.9645" dy="0.67" layer="1"/>
<smd name="9" x="2.4796" y="-4.445" dx="1.9645" dy="0.67" layer="1"/>
<smd name="10" x="2.4796" y="-3.175" dx="1.9645" dy="0.67" layer="1"/>
<smd name="11" x="2.4796" y="-1.905" dx="1.9645" dy="0.67" layer="1"/>
<smd name="12" x="2.4796" y="-0.635" dx="1.9645" dy="0.67" layer="1"/>
<smd name="13" x="2.4796" y="0.635" dx="1.9645" dy="0.67" layer="1"/>
<smd name="14" x="2.4796" y="1.905" dx="1.9645" dy="0.67" layer="1"/>
<smd name="15" x="2.4796" y="3.175" dx="1.9645" dy="0.67" layer="1"/>
<smd name="16" x="2.4796" y="4.445" dx="1.9645" dy="0.67" layer="1"/>
<text x="0" y="5.534" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.729" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-1.2446" y1="-5.133975" x2="-1.2446" y2="5.133975" width="0.127" layer="21"/>
<wire x1="-1.2446" y1="5.133975" x2="1.2446" y2="5.133975" width="0.127" layer="21"/>
<wire x1="1.2446" y1="5.133975" x2="1.2446" y2="-5.133975" width="0.127" layer="21"/>
<wire x1="1.2446" y1="-5.133975" x2="-1.2446" y2="-5.133975" width="0.127" layer="21"/>
</package>
<package name="HDRV4W64P254_1X4_1016X254X838B" urn="urn:adsk.eagle:footprint:7207461/1" locally_modified="yes">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.12" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.12" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="-1.27" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="1.27" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="3.81" y="0" drill="1.1051" diameter="1.7051"/>
<text x="-2.54" y="-2.421" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
</package>
<package name="TO455P987X240-3" urn="urn:adsk.eagle:footprint:7472989/1" locally_modified="yes">
<description>3-TO, DPAK, 4.55 mm pitch, 9.88 mm span, 6.54 X 6.09 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.55 mm pitch, 9.88 mm span with body size 6.54 X 6.09 X 2.40 mm&lt;/p&gt;</description>
<circle x="-4.3452" y="3.3005" radius="0.25" width="0" layer="21"/>
<wire x1="3.8325" y1="3.0374" x2="3.8325" y2="3.365" width="0.12" layer="21"/>
<wire x1="3.8325" y1="3.365" x2="-2.3875" y2="3.365" width="0.12" layer="21"/>
<wire x1="-2.3875" y1="3.365" x2="-2.3875" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-2.3875" y1="-3.365" x2="3.8325" y2="-3.365" width="0.12" layer="21"/>
<wire x1="3.8325" y1="-3.365" x2="3.8325" y2="-3.0374" width="0.12" layer="21"/>
<smd name="A" x="-4.3452" y="2.275" dx="2.4332" dy="1.0429" layer="1"/>
<smd name="A1" x="-4.3452" y="-2.275" dx="2.4332" dy="1.0429" layer="1"/>
<smd name="C" x="2.4182" y="0" dx="6.2871" dy="5.5669" layer="1"/>
<text x="0" y="4.1855" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="GEIER-R_TRIM">
<pad name="1" x="-2.54" y="0" drill="0.6" diameter="1.4224"/>
<pad name="3" x="2.54" y="0" drill="0.6" diameter="1.4224"/>
<pad name="2" x="0" y="-2.54" drill="0.6" diameter="1.4224"/>
<wire x1="3.81" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<text x="-3.81" y="3.175" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="0.8128" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="CAPAE660X550" urn="urn:adsk.eagle:footprint:7207783/1" locally_modified="yes">
<description>ECAP (Aluminum Electrolytic Capacitor), 6.60 X 5.50 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 6.60 X 5.50 mm&lt;/p&gt;</description>
<wire x1="-3.4" y1="1.079" x2="-3.4" y2="2.1125" width="0.12" layer="21"/>
<wire x1="-3.4" y1="2.1125" x2="-2.1125" y2="3.4" width="0.12" layer="21"/>
<wire x1="-2.1125" y1="3.4" x2="3.4" y2="3.4" width="0.12" layer="21"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="1.079" width="0.12" layer="21"/>
<wire x1="-3.4" y1="-1.079" x2="-3.4" y2="-2.1125" width="0.12" layer="21"/>
<wire x1="-3.4" y1="-2.1125" x2="-2.1125" y2="-3.4" width="0.12" layer="21"/>
<wire x1="-2.1125" y1="-3.4" x2="3.4" y2="-3.4" width="0.12" layer="21"/>
<wire x1="3.4" y1="-3.4" x2="3.4" y2="-1.079" width="0.12" layer="21"/>
<smd name="A" x="-2.6449" y="0" dx="3.5134" dy="1.65" layer="1"/>
<smd name="C" x="2.6449" y="0" dx="3.5134" dy="1.65" layer="1"/>
<text x="0" y="4.035" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.035" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPAE530X540" urn="urn:adsk.eagle:footprint:7492866/1" locally_modified="yes">
<description>ECAP (Aluminum Electrolytic Capacitor), 5.30 X 5.40 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 5.30 X 5.40 mm&lt;/p&gt;</description>
<wire x1="-2.65" y1="0.7658" x2="-2.65" y2="1.5809" width="0.12" layer="21"/>
<wire x1="-2.65" y1="1.5809" x2="-1.5809" y2="2.65" width="0.12" layer="21"/>
<wire x1="-1.5809" y1="2.65" x2="2.65" y2="2.65" width="0.12" layer="21"/>
<wire x1="2.65" y1="2.65" x2="2.65" y2="0.7658" width="0.12" layer="21"/>
<wire x1="-2.65" y1="-0.7658" x2="-2.65" y2="-1.5809" width="0.12" layer="21"/>
<wire x1="-2.65" y1="-1.5809" x2="-1.5809" y2="-2.65" width="0.12" layer="21"/>
<wire x1="-1.5809" y1="-2.65" x2="2.65" y2="-2.65" width="0.12" layer="21"/>
<wire x1="2.65" y1="-2.65" x2="2.65" y2="-0.7658" width="0.12" layer="21"/>
<smd name="A" x="-2.0989" y="0" dx="2.7214" dy="1.0236" layer="1"/>
<smd name="C" x="2.0989" y="0" dx="2.7214" dy="1.0236" layer="1"/>
<text x="0" y="3.285" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.285" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV5W64P170_1X5_1045X720X838B" urn="urn:adsk.eagle:footprint:7487798/1" locally_modified="yes">
<description>Single-row, 5-pin Pin Header (Male) Straight, 1.70 mm (0.07 in) col pitch, 5.84 mm mating length, 10.45 X 7.20 X 8.38 mm body
&lt;p&gt;Single-row (1X5), 5-pin Pin Header (Male) Straight package with 1.70 mm (0.07 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.45 X 7.20 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="5.25" y1="-1.06" x2="-5.25" y2="-1.06" width="0.12" layer="21"/>
<wire x1="-5.25" y1="-1.06" x2="-5.25" y2="6.14" width="0.12" layer="21"/>
<wire x1="-5.25" y1="6.14" x2="5.25" y2="6.14" width="0.12" layer="21"/>
<wire x1="5.25" y1="6.14" x2="5.25" y2="-1.06" width="0.12" layer="21"/>
<pad name="ON/OFF" x="-3.4" y="0" drill="1.07" diameter="1.4224"/>
<pad name="VIN" x="-1.7" y="0" drill="1.07" diameter="1.4224"/>
<pad name="GND" x="0" y="0" drill="1.07" diameter="1.4224"/>
<pad name="VOUT" x="1.7" y="0" drill="1.07" diameter="1.4224"/>
<pad name="TRIM" x="3.4" y="0" drill="1.07" diameter="1.4224"/>
<text x="0" y="4.989" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="2POL254" urn="urn:adsk.eagle:package:9320/1" locally_modified="yes" type="box">
<description>PHOENIX CONNECTOR</description>
<packageinstances>
<packageinstance name="2POL254"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X135" urn="urn:adsk.eagle:package:7158867/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.25 X 1.35 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X135"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:7130521/1" locally_modified="yes" type="model">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.90 X 3.90 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.90 X 3.90 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X70" urn="urn:adsk.eagle:package:7159533/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X70"/>
</packageinstances>
</package3d>
<package3d name="RESC2016X70" urn="urn:adsk.eagle:package:7158853/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2016X70"/>
</packageinstances>
</package3d>
<package3d name="DIOM4226X243" urn="urn:adsk.eagle:package:7188975/1" locally_modified="yes" type="model">
<description>Molded Body, 4.29 X 2.69 X 2.44 mm body
&lt;p&gt;Molded Body package with body size 4.29 X 2.69 X 2.44 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM4226X243"/>
</packageinstances>
</package3d>
<package3d name="HDRVR40W64P254_2X20_5080X508X254B" urn="urn:adsk.eagle:package:7166133/1" locally_modified="yes" type="model">
<description>Double-row, 40-pin Receptacle Header (Female) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 2.54 mm insulator length, 50.80 X 5.08 X 2.54 mm body
&lt;p&gt;Double-row (2X20), 40-pin Receptacle Header (Female) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 2.54 mm insulator length with overall size 50.80 X 5.08 X 2.54 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRVR40W64P254_2X20_5080X508X254B"/>
</packageinstances>
</package3d>
<package3d name="HDRV3W64P254_1X3_762X254X838B" urn="urn:adsk.eagle:package:7162108/1" locally_modified="yes" type="model">
<description>Single-row, 3-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X3), 3-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV3W64P254_1X3_762X254X838B"/>
</packageinstances>
</package3d>
<package3d name="SOP65P640X110-28" urn="urn:adsk.eagle:package:7189287/1" locally_modified="yes" type="model">
<description>28-SOP, 0.65 mm pitch, 6.40 mm span, 9.65 X 4.40 X 1.10 mm body
&lt;p&gt;28-pin SOP package with 0.65 mm pitch, 6.40 mm span with body size 9.65 X 4.40 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP65P640X110-28"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-16" urn="urn:adsk.eagle:package:7189502/1" locally_modified="yes" type="model">
<description>16-SOIC, 1.27 mm pitch, 6.00 mm span, 9.90 X 3.90 X 1.75 mm body
&lt;p&gt;16-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 9.90 X 3.90 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-16"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W64P254_1X4_1016X254X838B" urn="urn:adsk.eagle:package:7207460/1" locally_modified="yes" type="model">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X254X838B"/>
</packageinstances>
</package3d>
<package3d name="TO455P987X240-3" urn="urn:adsk.eagle:package:7472987/1" locally_modified="yes" type="model">
<description>3-TO, DPAK, 4.55 mm pitch, 9.88 mm span, 6.54 X 6.09 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.55 mm pitch, 9.88 mm span with body size 6.54 X 6.09 X 2.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO455P987X240-3"/>
</packageinstances>
</package3d>
<package3d name="CAPAE660X550" urn="urn:adsk.eagle:package:7207779/1" locally_modified="yes" type="model">
<description>ECAP (Aluminum Electrolytic Capacitor), 6.60 X 5.50 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 6.60 X 5.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPAE660X550"/>
</packageinstances>
</package3d>
<package3d name="CAPAE530X540" urn="urn:adsk.eagle:package:7492080/1" locally_modified="yes" type="model">
<description>ECAP (Aluminum Electrolytic Capacitor), 5.30 X 5.40 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 5.30 X 5.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPAE530X540"/>
</packageinstances>
</package3d>
<package3d name="HDRV5W64P170_1X5_1045X720X838B" urn="urn:adsk.eagle:package:7487796/1" locally_modified="yes" type="model">
<description>Single-row, 5-pin Pin Header (Male) Straight, 1.70 mm (0.07 in) col pitch, 5.84 mm mating length, 10.45 X 7.20 X 8.38 mm body
&lt;p&gt;Single-row (1X5), 5-pin Pin Header (Male) Straight package with 1.70 mm (0.07 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.45 X 7.20 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV5W64P170_1X5_1045X720X838B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="GEIER-FRAME_A3">
<wire x1="289.56" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="343.535" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="356.87" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="343.281" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="343.535" y="20.32" size="2.54" layer="94" font="vector">Geier Moritz</text>
<text x="343.535" y="10.16" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
</symbol>
<symbol name="GEIER-X2">
<wire x1="-2.54" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<pin name="+" x="5.08" y="2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="-" x="5.08" y="-2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<text x="-2.54" y="4.445" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="GEIER-C">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-0.635" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="-1.905" width="0.4064" layer="94"/>
<wire x1="0.635" y1="1.905" x2="0.635" y2="-1.905" width="0.4064" layer="94"/>
<text x="0" y="2.54" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-3.81" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-ACS711">
<pin name="IP+1" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="IP+2" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="IP-1" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="IP-2" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="VOUT" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="!FLT" x="10.16" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-6.35" x2="7.62" y2="-6.35" width="0.254" layer="94"/>
<wire x1="7.62" y1="-6.35" x2="7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="7.62" y1="6.35" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-PWR">
<pin name="+" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="-" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<wire x1="0" y1="5.133975" x2="0" y2="3.7338" width="0.1524" layer="94"/>
<circle x="0" y="2.644775" radius="0.996165625" width="0.254" layer="94"/>
<wire x1="0" y1="-5.133975" x2="0" y2="-3.7338" width="0.1524" layer="94"/>
<circle x="0" y="-2.644775" radius="0.946325" width="0.254" layer="94"/>
<wire x1="-2.17805" y1="8.0899" x2="-2.17805" y2="7.15645" width="0.1524" layer="97"/>
<wire x1="-2.644775" y1="7.623175" x2="-1.711325" y2="7.623175" width="0.1524" layer="97"/>
<text x="-1.5875" y="-0.9525" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="GEIER-D_LED">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="1.7526" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="-2.6924" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.00933125" y1="1.00933125" x2="0.367034375" y2="1.651634375" width="0.127" layer="94"/>
<wire x1="0.642303125" y1="0.6423" x2="0" y2="1.284603125" width="0.127" layer="94"/>
<polygon width="0.127" layer="94">
<vertex x="0" y="1.28460625"/>
<vertex x="0" y="1.101090625"/>
<vertex x="0.183515625" y="1.28460625"/>
</polygon>
<polygon width="0.127" layer="94">
<vertex x="0.36703125" y="1.651634375"/>
<vertex x="0.36703125" y="1.46811875"/>
<vertex x="0.55054375" y="1.65163125"/>
<vertex x="0.55054375" y="1.651634375"/>
</polygon>
</symbol>
<symbol name="GEIER-R">
<wire x1="-2.54" y1="0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.2225" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="1.7526" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="-2.6924" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="GEIER-PI_EEPROM">
<pin name="ID_SD" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="ID_SC" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-PI_I2C">
<pin name="SDA" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="SCL" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-PI_SPI">
<pin name="MISO" x="5.08" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="MOSI" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="SCLK" x="5.08" y="0" visible="pin" length="short" rot="R180"/>
<pin name="CE0" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="CE1" x="5.08" y="-5.08" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-7.62" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-6.35" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-PI_UART">
<pin name="TX" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="RX" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-PI_GPIO">
<pin name="GPIO17" x="5.08" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="GPIO18" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="GPIO19" x="5.08" y="0" visible="pin" length="short" rot="R180"/>
<pin name="GPIO20" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GPIO21" x="5.08" y="-5.08" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="21.59" x2="2.54" y2="-21.59" width="0.254" layer="94"/>
<wire x1="2.54" y1="-21.59" x2="-7.62" y2="-21.59" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-21.59" x2="-7.62" y2="21.59" width="0.254" layer="94"/>
<wire x1="-7.62" y1="21.59" x2="2.54" y2="21.59" width="0.254" layer="94"/>
<text x="-7.62" y="22.86" size="0.8128" layer="95">&gt;NAME</text>
<pin name="GPIO22" x="5.08" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="GPIO16" x="5.08" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="GPIO13" x="5.08" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="GPIO12" x="5.08" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="GPIO23" x="5.08" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="GPIO24" x="5.08" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="GPIO6" x="5.08" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="GPIO25" x="5.08" y="-15.24" visible="pin" length="short" rot="R180"/>
<pin name="GPIO5" x="5.08" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="GPIO26" x="5.08" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="GPI04" x="5.08" y="20.32" visible="pin" length="short" rot="R180"/>
<pin name="GPIO27" x="5.08" y="-20.32" visible="pin" length="short" rot="R180"/>
</symbol>
<symbol name="GEIER-PI_PWROUT">
<pin name="+3V3" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="+5V" x="5.08" y="0" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-HDR3">
<pin name="P$1" x="2.54" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="P$2" x="0" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="P$3" x="-2.54" y="2.54" visible="pad" length="short" rot="R270"/>
<wire x1="-3.175" y1="0" x2="3.175" y2="0" width="0.254" layer="94"/>
<text x="-3.175" y="-1.27" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="GEIER-PCA9685">
<pin name="SCL" x="-12.7" y="15.24" visible="pin" length="short" direction="in"/>
<pin name="SDA" x="-12.7" y="12.7" visible="pin" length="short"/>
<pin name="A0" x="-12.7" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="A1" x="-12.7" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="A2" x="-12.7" y="2.54" visible="pin" length="short" direction="in"/>
<pin name="A3" x="-12.7" y="0" visible="pin" length="short" direction="in"/>
<pin name="A4" x="-12.7" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="EXTCLK" x="-12.7" y="-12.7" visible="pin" length="short" direction="in"/>
<pin name="A5" x="-12.7" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="!OE" x="-12.7" y="-10.16" visible="pin" length="short" direction="in"/>
<pin name="LED0" x="12.7" y="15.24" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED1" x="12.7" y="12.7" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED2" x="12.7" y="10.16" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED3" x="12.7" y="7.62" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED4" x="12.7" y="5.08" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED5" x="12.7" y="2.54" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED6" x="12.7" y="0" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED7" x="12.7" y="-2.54" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED8" x="12.7" y="-5.08" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED9" x="12.7" y="-7.62" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED10" x="12.7" y="-10.16" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED11" x="12.7" y="-12.7" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED12" x="12.7" y="-15.24" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED13" x="12.7" y="-17.78" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED14" x="12.7" y="-20.32" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="LED15" x="12.7" y="-22.86" visible="pin" length="short" direction="in" rot="R180"/>
<wire x1="-10.16" y1="16.51" x2="-10.16" y2="-24.13" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-24.13" x2="10.16" y2="-24.13" width="0.254" layer="94"/>
<wire x1="10.16" y1="-24.13" x2="10.16" y2="16.51" width="0.254" layer="94"/>
<wire x1="10.16" y1="16.51" x2="-10.16" y2="16.51" width="0.254" layer="94"/>
<text x="-10.16" y="17.78" size="0.8128" layer="95">&gt;NAME</text>
<text x="-10.16" y="-25.4" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-MCP3008">
<pin name="CH0" x="-5.08" y="15.24" visible="pin" length="short" direction="in"/>
<pin name="CH1" x="-5.08" y="12.7" visible="pin" length="short" direction="in"/>
<pin name="CH2" x="-5.08" y="10.16" visible="pin" length="short" direction="in"/>
<pin name="CH3" x="-5.08" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="CH4" x="-5.08" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="CH5" x="-5.08" y="2.54" visible="pin" length="short" direction="in"/>
<pin name="CH6" x="-5.08" y="0" visible="pin" length="short" direction="in"/>
<pin name="CH7" x="-5.08" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="MISO" x="-5.08" y="-10.16" visible="pin" length="short" direction="in"/>
<pin name="MOSI" x="-5.08" y="-12.7" visible="pin" length="short" direction="in"/>
<pin name="!CS" x="-5.08" y="-15.24" visible="pin" length="short" direction="in"/>
<pin name="SCLK" x="-5.08" y="-7.62" visible="pin" length="short" direction="in"/>
<wire x1="-2.54" y1="16.51" x2="-2.54" y2="-16.51" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-16.51" x2="7.62" y2="-16.51" width="0.254" layer="94"/>
<wire x1="7.62" y1="-16.51" x2="7.62" y2="16.51" width="0.254" layer="94"/>
<wire x1="7.62" y1="16.51" x2="-2.54" y2="16.51" width="0.254" layer="94"/>
<text x="-2.54" y="17.78" size="0.8128" layer="95">&gt;NAME</text>
<text x="-2.54" y="-17.78" size="0.8128" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-HDR4">
<pin name="P$1" x="-5.08" y="2.54" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="-2.54" y="2.54" visible="off" length="short" rot="R270"/>
<pin name="P$3" x="2.54" y="2.54" visible="off" length="short" rot="R270"/>
<wire x1="-5.715" y1="0" x2="5.715" y2="0" width="0.254" layer="94"/>
<text x="-5.715" y="-1.27" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="P$4" x="5.08" y="2.54" visible="off" length="short" rot="R270"/>
</symbol>
<symbol name="GEIER-R_TRIM">
<wire x1="-2.54" y1="0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="3" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="0.3175" y="-2.2225" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
<wire x1="0" y1="-1.27" x2="2.8575" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="2.8575" y1="1.5875" x2="2.54" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.5875" x2="2.8575" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.8575" y1="1.27" x2="2.8575" y2="1.5875" width="0.1524" layer="94"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
</symbol>
<symbol name="GEIER-C_POL">
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.905" width="0.4064" layer="94"/>
<wire x1="0.635" y1="-1.905" x2="0.635" y2="0" width="0.4064" layer="94"/>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="in"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="out" rot="R180"/>
<wire x1="0.635" y1="0" x2="0.635" y2="1.905" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-2.921" y1="2.032" x2="-1.905" y2="2.032" width="0.1524" layer="94"/>
<text x="-1.27" y="2.54" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-NQR01A0X">
<pin name="ON/OFF" x="-5.08" y="5.08" visible="pin" length="short"/>
<pin name="VIN" x="-5.08" y="2.54" visible="pin" length="short"/>
<pin name="GND" x="-5.08" y="0" visible="pin" length="short"/>
<pin name="TRIM" x="-5.08" y="-5.08" visible="pin" length="short"/>
<pin name="VOUT" x="-5.08" y="-2.54" visible="pin" length="short"/>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="7.62" y2="-6.35" width="0.254" layer="94"/>
<wire x1="7.62" y1="-6.35" x2="7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="7.62" y1="6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GEIER-FRAME_A3">
<gates>
<gate name="G$1" symbol="GEIER-FRAME_A3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-X2" prefix="X">
<gates>
<gate name="G$1" symbol="GEIER-X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2POL254">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9320/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-C" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="CAPC2012X135">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7158867/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-ACS711" prefix="IC">
<gates>
<gate name="G$1" symbol="GEIER-ACS711" x="0" y="0"/>
<gate name="PWR" symbol="GEIER-PWR" x="-17.78" y="0" addlevel="request"/>
</gates>
<devices>
<device name="SOIC" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="!FLT" pad="6"/>
<connect gate="G$1" pin="IP+1" pad="1"/>
<connect gate="G$1" pin="IP+2" pad="2"/>
<connect gate="G$1" pin="IP-1" pad="3"/>
<connect gate="G$1" pin="IP-2" pad="4"/>
<connect gate="G$1" pin="VOUT" pad="7"/>
<connect gate="PWR" pin="+" pad="8"/>
<connect gate="PWR" pin="-" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7130521/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-LED" prefix="LED" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-D_LED" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="LEDC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7159533/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-R" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="RESC2016X70">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7158853/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-D" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-D" x="0" y="0"/>
</gates>
<devices>
<device name="1N4007" package="DIOM4226X243">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7188975/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FRED" package="TO455P987X240-3">
<connects>
<connect gate="G$1" pin="A" pad="A A1"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7472987/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-PI3B+" prefix="PI">
<gates>
<gate name="PWR" symbol="GEIER-PWR" x="-12.7" y="0" addlevel="request"/>
<gate name="EEPROM" symbol="GEIER-PI_EEPROM" x="5.08" y="25.4"/>
<gate name="I2C" symbol="GEIER-PI_I2C" x="5.08" y="12.7"/>
<gate name="SPI" symbol="GEIER-PI_SPI" x="5.08" y="-2.54"/>
<gate name="UART" symbol="GEIER-PI_UART" x="5.08" y="-17.78"/>
<gate name="GPIO" symbol="GEIER-PI_GPIO" x="25.4" y="5.08"/>
<gate name="PWROUT" symbol="GEIER-PI_PWROUT" x="5.08" y="-30.48"/>
</gates>
<devices>
<device name="PI_3B+" package="HDRVR40W64P254_2X20_5080X508X254B">
<connects>
<connect gate="EEPROM" pin="ID_SC" pad="28"/>
<connect gate="EEPROM" pin="ID_SD" pad="27"/>
<connect gate="GPIO" pin="GPI04" pad="7"/>
<connect gate="GPIO" pin="GPIO12" pad="32"/>
<connect gate="GPIO" pin="GPIO13" pad="33"/>
<connect gate="GPIO" pin="GPIO16" pad="36"/>
<connect gate="GPIO" pin="GPIO17" pad="11"/>
<connect gate="GPIO" pin="GPIO18" pad="12"/>
<connect gate="GPIO" pin="GPIO19" pad="35"/>
<connect gate="GPIO" pin="GPIO20" pad="38"/>
<connect gate="GPIO" pin="GPIO21" pad="40"/>
<connect gate="GPIO" pin="GPIO22" pad="15"/>
<connect gate="GPIO" pin="GPIO23" pad="16"/>
<connect gate="GPIO" pin="GPIO24" pad="18"/>
<connect gate="GPIO" pin="GPIO25" pad="22"/>
<connect gate="GPIO" pin="GPIO26" pad="37"/>
<connect gate="GPIO" pin="GPIO27" pad="13"/>
<connect gate="GPIO" pin="GPIO5" pad="29"/>
<connect gate="GPIO" pin="GPIO6" pad="31"/>
<connect gate="I2C" pin="SCL" pad="5"/>
<connect gate="I2C" pin="SDA" pad="3"/>
<connect gate="PWR" pin="+" pad="2"/>
<connect gate="PWR" pin="-" pad="6"/>
<connect gate="PWROUT" pin="+3V3" pad="1 17"/>
<connect gate="PWROUT" pin="+5V" pad="4"/>
<connect gate="PWROUT" pin="GND" pad="9 14 20 25 30 34 39"/>
<connect gate="SPI" pin="CE0" pad="24"/>
<connect gate="SPI" pin="CE1" pad="26"/>
<connect gate="SPI" pin="MISO" pad="21"/>
<connect gate="SPI" pin="MOSI" pad="19"/>
<connect gate="SPI" pin="SCLK" pad="23"/>
<connect gate="UART" pin="RX" pad="10"/>
<connect gate="UART" pin="TX" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7166133/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-HDR3" prefix="HDR">
<gates>
<gate name="G$1" symbol="GEIER-HDR3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV3W64P254_1X3_762X254X838B">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7162108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-PCA9685" prefix="IC">
<gates>
<gate name="PWR" symbol="GEIER-PWR" x="-17.78" y="0" addlevel="request"/>
<gate name="G$2" symbol="GEIER-PCA9685" x="0" y="0"/>
</gates>
<devices>
<device name="TSSOP28" package="SOP65P640X110-28">
<connects>
<connect gate="G$2" pin="!OE" pad="23"/>
<connect gate="G$2" pin="A0" pad="1"/>
<connect gate="G$2" pin="A1" pad="2"/>
<connect gate="G$2" pin="A2" pad="3"/>
<connect gate="G$2" pin="A3" pad="4"/>
<connect gate="G$2" pin="A4" pad="5"/>
<connect gate="G$2" pin="A5" pad="24"/>
<connect gate="G$2" pin="EXTCLK" pad="25"/>
<connect gate="G$2" pin="LED0" pad="6"/>
<connect gate="G$2" pin="LED1" pad="7"/>
<connect gate="G$2" pin="LED10" pad="17"/>
<connect gate="G$2" pin="LED11" pad="18"/>
<connect gate="G$2" pin="LED12" pad="19"/>
<connect gate="G$2" pin="LED13" pad="20"/>
<connect gate="G$2" pin="LED14" pad="21"/>
<connect gate="G$2" pin="LED15" pad="22"/>
<connect gate="G$2" pin="LED2" pad="8"/>
<connect gate="G$2" pin="LED3" pad="9"/>
<connect gate="G$2" pin="LED4" pad="10"/>
<connect gate="G$2" pin="LED5" pad="11"/>
<connect gate="G$2" pin="LED6" pad="12"/>
<connect gate="G$2" pin="LED7" pad="13"/>
<connect gate="G$2" pin="LED8" pad="15"/>
<connect gate="G$2" pin="LED9" pad="16"/>
<connect gate="G$2" pin="SCL" pad="26"/>
<connect gate="G$2" pin="SDA" pad="27"/>
<connect gate="PWR" pin="+" pad="28"/>
<connect gate="PWR" pin="-" pad="14"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7189287/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-MCP3008" prefix="IC">
<gates>
<gate name="DPWR" symbol="GEIER-PWR" x="-17.78" y="0" addlevel="request"/>
<gate name="APWR" symbol="GEIER-PWR" x="-25.4" y="0" addlevel="request"/>
<gate name="G$1" symbol="GEIER-MCP3008" x="0" y="0"/>
</gates>
<devices>
<device name="SOIC" package="SOIC127P600X175-16">
<connects>
<connect gate="APWR" pin="+" pad="15"/>
<connect gate="APWR" pin="-" pad="14"/>
<connect gate="DPWR" pin="+" pad="16"/>
<connect gate="DPWR" pin="-" pad="9"/>
<connect gate="G$1" pin="!CS" pad="10"/>
<connect gate="G$1" pin="CH0" pad="1"/>
<connect gate="G$1" pin="CH1" pad="2"/>
<connect gate="G$1" pin="CH2" pad="3"/>
<connect gate="G$1" pin="CH3" pad="4"/>
<connect gate="G$1" pin="CH4" pad="5"/>
<connect gate="G$1" pin="CH5" pad="6"/>
<connect gate="G$1" pin="CH6" pad="7"/>
<connect gate="G$1" pin="CH7" pad="8"/>
<connect gate="G$1" pin="MISO" pad="12"/>
<connect gate="G$1" pin="MOSI" pad="11"/>
<connect gate="G$1" pin="SCLK" pad="13"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7189502/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-HDR4" prefix="HDR">
<gates>
<gate name="G$1" symbol="GEIER-HDR4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV4W64P254_1X4_1016X254X838B">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7207460/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-R_TRIM" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-R_TRIM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GEIER-R_TRIM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-C_POL" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-C_POL" x="0" y="0"/>
</gates>
<devices>
<device name="6.6MM" package="CAPAE660X550">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7207779/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="CAPAE530X540">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7492080/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-NQR01A0X" prefix="VREG">
<gates>
<gate name="G$1" symbol="GEIER-NQR01A0X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV5W64P170_1X5_1045X720X838B">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ON/OFF" pad="ON/OFF"/>
<connect gate="G$1" pin="TRIM" pad="TRIM"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VOUT" pad="VOUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7487796/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="RobticArm" deviceset="GEIER-FRAME_A3" device=""/>
<part name="VCC" library="RobticArm" deviceset="GEIER-X2" device="" package3d_urn="urn:adsk.eagle:package:9320/1"/>
<part name="C1" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="C2" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="C3" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="C4" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="C5" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="IC1" library="RobticArm" deviceset="GEIER-ACS711" device="SOIC" package3d_urn="urn:adsk.eagle:package:7130521/1" value="ACS711"/>
<part name="IC2" library="RobticArm" deviceset="GEIER-ACS711" device="SOIC" package3d_urn="urn:adsk.eagle:package:7130521/1" value="ACS711"/>
<part name="IC3" library="RobticArm" deviceset="GEIER-ACS711" device="SOIC" package3d_urn="urn:adsk.eagle:package:7130521/1" value="ACS711"/>
<part name="IC4" library="RobticArm" deviceset="GEIER-ACS711" device="SOIC" package3d_urn="urn:adsk.eagle:package:7130521/1" value="ACS711"/>
<part name="IC5" library="RobticArm" deviceset="GEIER-ACS711" device="SOIC" package3d_urn="urn:adsk.eagle:package:7130521/1" value="ACS711"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="PWR" library="RobticArm" deviceset="GEIER-LED" device="0805" package3d_urn="urn:adsk.eagle:package:7159533/1" value="GRN"/>
<part name="R1" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="270"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="D1" library="RobticArm" deviceset="GEIER-D" device="FRED" package3d_urn="urn:adsk.eagle:package:7472987/1" value="FRED20S100S"/>
<part name="PI1" library="RobticArm" deviceset="GEIER-PI3B+" device="PI_3B+" package3d_urn="urn:adsk.eagle:package:7166133/1"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SERVO0" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="SERVO1" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="SERVO2" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="SERVO3" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="SERVO4" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC6" library="RobticArm" deviceset="GEIER-PCA9685" device="TSSOP28" package3d_urn="urn:adsk.eagle:package:7189287/1" value="PCA9685"/>
<part name="C6" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC7" library="RobticArm" deviceset="GEIER-MCP3008" device="SOIC" package3d_urn="urn:adsk.eagle:package:7189502/1" value="MCP3008SOIC"/>
<part name="RF-UART" library="RobticArm" deviceset="GEIER-HDR4" device="" package3d_urn="urn:adsk.eagle:package:7207460/1"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="PWM0" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM1" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM2" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM3" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM4" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM5" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM6" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM7" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM8" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM9" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="PWM10" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C7" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="C8" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="HDR1" library="RobticArm" deviceset="GEIER-HDR3" device="" package3d_urn="urn:adsk.eagle:package:7162108/1"/>
<part name="HDR2" library="RobticArm" deviceset="GEIER-HDR4" device="" package3d_urn="urn:adsk.eagle:package:7207460/1"/>
<part name="HDR3" library="RobticArm" deviceset="GEIER-HDR4" device="" package3d_urn="urn:adsk.eagle:package:7207460/1"/>
<part name="HDR4" library="RobticArm" deviceset="GEIER-HDR4" device="" package3d_urn="urn:adsk.eagle:package:7207460/1"/>
<part name="R2" library="RobticArm" deviceset="GEIER-R_TRIM" device="" value="1k"/>
<part name="C9" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="39nF"/>
<part name="R3" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="47"/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C10" library="RobticArm" deviceset="GEIER-C_POL" device="5MM" package3d_urn="urn:adsk.eagle:package:7492080/1" value="47µF"/>
<part name="C11" library="RobticArm" deviceset="GEIER-C_POL" device="5MM" package3d_urn="urn:adsk.eagle:package:7492080/1" value="47µF"/>
<part name="C12" library="RobticArm" deviceset="GEIER-C_POL" device="5MM" package3d_urn="urn:adsk.eagle:package:7492080/1" value="47µF"/>
<part name="C13" library="RobticArm" deviceset="GEIER-C_POL" device="5MM" package3d_urn="urn:adsk.eagle:package:7492080/1" value="47µF"/>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R4" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="NC"/>
<part name="VREG1" library="RobticArm" deviceset="GEIER-NQR01A0X" device="" package3d_urn="urn:adsk.eagle:package:7487796/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="343.535" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="SHEET" x="356.87" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="343.535" y="10.16" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="VCC" gate="G$1" x="7.62" y="231.14" smashed="yes">
<attribute name="NAME" x="5.08" y="235.585" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="C1" gate="G$1" x="93.98" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="91.44" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="97.79" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="109.22" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="106.68" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="113.03" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="124.46" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="121.92" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="128.27" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="139.7" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="143.51" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="154.94" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="152.4" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="158.75" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="IC1" gate="G$1" x="177.8" y="66.04" smashed="yes">
<attribute name="NAME" x="170.18" y="73.66" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC2" gate="G$1" x="218.44" y="66.04" smashed="yes">
<attribute name="NAME" x="210.82" y="73.66" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC3" gate="G$1" x="259.08" y="66.04" smashed="yes">
<attribute name="NAME" x="251.46" y="73.66" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC4" gate="G$1" x="299.72" y="66.04" smashed="yes">
<attribute name="NAME" x="292.1" y="73.66" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC5" gate="G$1" x="340.36" y="66.04" smashed="yes">
<attribute name="NAME" x="332.74" y="73.66" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="IC1" gate="PWR" x="88.9" y="231.14" smashed="yes">
<attribute name="NAME" x="87.3125" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC2" gate="PWR" x="104.14" y="231.14" smashed="yes">
<attribute name="NAME" x="102.5525" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC3" gate="PWR" x="119.38" y="231.14" smashed="yes">
<attribute name="NAME" x="117.7925" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC4" gate="PWR" x="134.62" y="231.14" smashed="yes">
<attribute name="NAME" x="133.0325" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC5" gate="PWR" x="149.86" y="231.14" smashed="yes">
<attribute name="NAME" x="148.2725" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="15.24" y="215.9" smashed="yes">
<attribute name="VALUE" x="12.7" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="88.9" y="215.9" smashed="yes">
<attribute name="VALUE" x="86.36" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="104.14" y="215.9" smashed="yes">
<attribute name="VALUE" x="101.6" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="119.38" y="215.9" smashed="yes">
<attribute name="VALUE" x="116.84" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="134.62" y="215.9" smashed="yes">
<attribute name="VALUE" x="132.08" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="149.86" y="215.9" smashed="yes">
<attribute name="VALUE" x="147.32" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="223.52" y="215.9" smashed="yes">
<attribute name="VALUE" x="220.98" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="PWR" gate="G$1" x="223.52" y="226.06" smashed="yes" rot="R270">
<attribute name="NAME" x="225.2726" y="228.6" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="220.8276" y="228.6" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="R1" gate="G$1" x="223.52" y="236.22" smashed="yes" rot="R270">
<attribute name="NAME" x="224.79" y="238.76" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="221.2975" y="238.76" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="P+1" gate="1" x="223.52" y="248.92" smashed="yes">
<attribute name="VALUE" x="220.98" y="246.38" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D1" gate="G$1" x="17.78" y="233.68" smashed="yes">
<attribute name="NAME" x="15.24" y="235.4326" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="15.24" y="230.9876" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="PI1" gate="PWR" x="165.1" y="231.14" smashed="yes">
<attribute name="NAME" x="163.5125" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="GND9" gate="1" x="165.1" y="215.9" smashed="yes">
<attribute name="VALUE" x="162.56" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="PI1" gate="GPIO" x="33.02" y="53.34" smashed="yes">
<attribute name="NAME" x="25.4" y="76.2" size="0.8128" layer="95"/>
</instance>
<instance part="SERVO0" gate="G$1" x="154.94" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="153.67" y="64.135" size="0.8128" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="SERVO1" gate="G$1" x="195.58" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="194.31" y="64.135" size="0.8128" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="SERVO2" gate="G$1" x="236.22" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="234.95" y="64.135" size="0.8128" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="SERVO3" gate="G$1" x="276.86" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="275.59" y="64.135" size="0.8128" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="SERVO4" gate="G$1" x="317.5" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="316.23" y="64.135" size="0.8128" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="P+2" gate="1" x="165.1" y="81.28" smashed="yes">
<attribute name="VALUE" x="162.56" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="1" x="205.74" y="81.28" smashed="yes">
<attribute name="VALUE" x="203.2" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="1" x="246.38" y="81.28" smashed="yes">
<attribute name="VALUE" x="243.84" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+5" gate="1" x="287.02" y="81.28" smashed="yes">
<attribute name="VALUE" x="284.48" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+6" gate="1" x="327.66" y="81.28" smashed="yes">
<attribute name="VALUE" x="325.12" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="160.02" y="53.34" smashed="yes">
<attribute name="VALUE" x="157.48" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="200.66" y="53.34" smashed="yes">
<attribute name="VALUE" x="198.12" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="241.3" y="53.34" smashed="yes">
<attribute name="VALUE" x="238.76" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="281.94" y="53.34" smashed="yes">
<attribute name="VALUE" x="279.4" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="322.58" y="53.34" smashed="yes">
<attribute name="VALUE" x="320.04" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="IC6" gate="G$2" x="119.38" y="68.58" smashed="yes">
<attribute name="NAME" x="109.22" y="86.36" size="0.8128" layer="95"/>
<attribute name="VALUE" x="109.22" y="43.18" size="0.8128" layer="96"/>
</instance>
<instance part="IC6" gate="PWR" x="180.34" y="231.14" smashed="yes">
<attribute name="NAME" x="178.7525" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="185.42" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="182.88" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="189.23" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND14" gate="1" x="180.34" y="215.9" smashed="yes">
<attribute name="VALUE" x="177.8" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="PI1" gate="I2C" x="33.02" y="81.28" smashed="yes">
<attribute name="NAME" x="25.4" y="86.36" size="0.8128" layer="95"/>
</instance>
<instance part="IC7" gate="G$1" x="370.84" y="53.34" smashed="yes">
<attribute name="NAME" x="368.3" y="71.12" size="0.8128" layer="95"/>
<attribute name="VALUE" x="368.3" y="35.56" size="0.8128" layer="95"/>
</instance>
<instance part="PI1" gate="SPI" x="33.02" y="22.86" smashed="yes">
<attribute name="NAME" x="25.4" y="30.48" size="0.8128" layer="95"/>
</instance>
<instance part="RF-UART" gate="G$1" x="88.9" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="90.17" y="12.065" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="+3V3" gate="G$1" x="81.28" y="17.78" smashed="yes">
<attribute name="VALUE" x="78.74" y="12.7" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND15" gate="1" x="83.82" y="7.62" smashed="yes">
<attribute name="VALUE" x="81.28" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="PI1" gate="UART" x="33.02" y="10.16" smashed="yes">
<attribute name="NAME" x="25.4" y="15.24" size="0.8128" layer="95"/>
</instance>
<instance part="PI1" gate="PWROUT" x="33.02" y="116.84" smashed="yes">
<attribute name="NAME" x="25.4" y="121.92" size="0.8128" layer="95"/>
</instance>
<instance part="GND16" gate="1" x="40.64" y="101.6" smashed="yes">
<attribute name="VALUE" x="38.1" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="+3V4" gate="G$1" x="40.64" y="132.08" smashed="yes">
<attribute name="VALUE" x="38.1" y="129.54" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PWM0" gate="G$1" x="160.02" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="161.29" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM1" gate="G$1" x="175.26" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="176.53" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM2" gate="G$1" x="190.5" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="191.77" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM3" gate="G$1" x="203.2" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="204.47" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM4" gate="G$1" x="215.9" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="217.17" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM5" gate="G$1" x="228.6" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="229.87" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM6" gate="G$1" x="241.3" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="242.57" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM7" gate="G$1" x="254" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="255.27" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM8" gate="G$1" x="269.24" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="270.51" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM9" gate="G$1" x="284.48" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="285.75" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="PWM10" gate="G$1" x="302.26" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="303.53" y="103.505" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="P+7" gate="1" x="154.94" y="114.3" smashed="yes">
<attribute name="VALUE" x="152.4" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+8" gate="1" x="170.18" y="114.3" smashed="yes">
<attribute name="VALUE" x="167.64" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+9" gate="1" x="185.42" y="114.3" smashed="yes">
<attribute name="VALUE" x="182.88" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+10" gate="1" x="198.12" y="114.3" smashed="yes">
<attribute name="VALUE" x="195.58" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+11" gate="1" x="210.82" y="114.3" smashed="yes">
<attribute name="VALUE" x="208.28" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+12" gate="1" x="223.52" y="114.3" smashed="yes">
<attribute name="VALUE" x="220.98" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+13" gate="1" x="236.22" y="114.3" smashed="yes">
<attribute name="VALUE" x="233.68" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+14" gate="1" x="248.92" y="114.3" smashed="yes">
<attribute name="VALUE" x="246.38" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+15" gate="1" x="264.16" y="114.3" smashed="yes">
<attribute name="VALUE" x="261.62" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+16" gate="1" x="279.4" y="114.3" smashed="yes">
<attribute name="VALUE" x="276.86" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+17" gate="1" x="297.18" y="114.3" smashed="yes">
<attribute name="VALUE" x="294.64" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND17" gate="1" x="294.64" y="101.6" smashed="yes">
<attribute name="VALUE" x="292.1" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="276.86" y="101.6" smashed="yes">
<attribute name="VALUE" x="274.32" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="261.62" y="101.6" smashed="yes">
<attribute name="VALUE" x="259.08" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="246.38" y="101.6" smashed="yes">
<attribute name="VALUE" x="243.84" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="233.68" y="101.6" smashed="yes">
<attribute name="VALUE" x="231.14" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND22" gate="1" x="220.98" y="101.6" smashed="yes">
<attribute name="VALUE" x="218.44" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="208.28" y="101.6" smashed="yes">
<attribute name="VALUE" x="205.74" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="195.58" y="101.6" smashed="yes">
<attribute name="VALUE" x="193.04" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="182.88" y="101.6" smashed="yes">
<attribute name="VALUE" x="180.34" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="167.64" y="101.6" smashed="yes">
<attribute name="VALUE" x="165.1" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="152.4" y="101.6" smashed="yes">
<attribute name="VALUE" x="149.86" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="IC7" gate="APWR" x="195.58" y="231.14" smashed="yes">
<attribute name="NAME" x="193.9925" y="230.1875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="IC7" gate="DPWR" x="50.8" y="116.84" smashed="yes">
<attribute name="NAME" x="49.2125" y="115.8875" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="GND28" gate="1" x="50.8" y="101.6" smashed="yes">
<attribute name="VALUE" x="48.26" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="55.88" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="53.34" y="116.84" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="59.69" y="116.84" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="200.66" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="198.12" y="231.14" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="204.47" y="231.14" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND29" gate="1" x="195.58" y="215.9" smashed="yes">
<attribute name="VALUE" x="193.04" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="HDR1" gate="G$1" x="111.76" y="20.32" smashed="yes">
<attribute name="NAME" x="108.585" y="19.05" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="HDR2" gate="G$1" x="76.2" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="77.47" y="60.325" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="HDR3" gate="G$1" x="76.2" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="77.47" y="47.625" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="HDR4" gate="G$1" x="76.2" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="77.47" y="34.925" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="25.4" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="177.8" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="27.6225" y="180.6575" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="25.4" y="195.58" smashed="yes" rot="R90">
<attribute name="NAME" x="22.86" y="195.58" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="29.21" y="195.58" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="25.4" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="205.74" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="27.6225" y="205.74" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND30" gate="1" x="22.86" y="215.9" smashed="yes">
<attribute name="VALUE" x="20.32" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="25.4" y="167.64" smashed="yes">
<attribute name="VALUE" x="22.86" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="76.2" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="78.74" y="232.41" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="73.025" y="232.41" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="C11" gate="G$1" x="53.34" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="55.88" y="232.41" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="50.165" y="232.41" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="C12" gate="G$1" x="60.96" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="63.5" y="232.41" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="57.785" y="232.41" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="C13" gate="G$1" x="68.58" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="71.12" y="232.41" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="65.405" y="232.41" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="GND32" gate="1" x="53.34" y="215.9" smashed="yes">
<attribute name="VALUE" x="50.8" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND33" gate="1" x="60.96" y="215.9" smashed="yes">
<attribute name="VALUE" x="58.42" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND34" gate="1" x="68.58" y="215.9" smashed="yes">
<attribute name="VALUE" x="66.04" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="GND35" gate="1" x="76.2" y="215.9" smashed="yes">
<attribute name="VALUE" x="73.66" y="213.36" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="35.56" y="243.84" smashed="yes">
<attribute name="NAME" x="33.02" y="245.11" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="33.02" y="241.6175" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="VREG1" gate="G$1" x="35.56" y="231.14" smashed="yes">
<attribute name="NAME" x="33.02" y="238.76" size="0.8128" layer="95"/>
</instance>
</instances>
<busses>
<bus name="A[0..5],!OE">
<segment>
<wire x1="40.64" y1="58.42" x2="40.64" y2="76.2" width="0.762" layer="92"/>
<wire x1="40.64" y1="76.2" x2="104.14" y2="76.2" width="0.762" layer="92"/>
<wire x1="104.14" y1="76.2" x2="104.14" y2="58.42" width="0.762" layer="92"/>
<label x="66.04" y="73.66" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="SERVO[0..4]">
<segment>
<wire x1="134.62" y1="73.66" x2="134.62" y2="83.82" width="0.762" layer="92"/>
<wire x1="134.62" y1="83.82" x2="322.58" y2="83.82" width="0.762" layer="92"/>
<label x="187.96" y="86.36" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="SPI:CLK,CS,MISO,MOSI">
<segment>
<wire x1="40.64" y1="20.32" x2="40.64" y2="27.94" width="0.762" layer="92"/>
<wire x1="40.64" y1="27.94" x2="363.22" y2="27.94" width="0.762" layer="92"/>
<wire x1="363.22" y1="27.94" x2="363.22" y2="45.72" width="0.762" layer="92"/>
<label x="68.58" y="25.4" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="AREAD[0..4]">
<segment>
<wire x1="358.14" y1="68.58" x2="358.14" y2="45.72" width="0.762" layer="92"/>
<wire x1="358.14" y1="45.72" x2="190.5" y2="45.72" width="0.762" layer="92"/>
<label x="220.98" y="43.18" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="PWM[0..10]">
<segment>
<wire x1="134.62" y1="45.72" x2="134.62" y2="71.12" width="0.762" layer="92"/>
<wire x1="134.62" y1="71.12" x2="137.16" y2="71.12" width="0.762" layer="92"/>
<wire x1="137.16" y1="71.12" x2="137.16" y2="96.52" width="0.762" layer="92"/>
<wire x1="137.16" y1="96.52" x2="297.18" y2="96.52" width="0.762" layer="92"/>
<label x="187.96" y="93.98" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="PWR" pin="-"/>
<wire x1="88.9" y1="223.52" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="P$1"/>
<wire x1="93.98" y1="226.06" x2="93.98" y2="220.98" width="0.1524" layer="91"/>
<wire x1="93.98" y1="220.98" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="88.9" y1="218.44" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<junction x="88.9" y="220.98"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="VCC" gate="G$1" pin="-"/>
<wire x1="15.24" y1="218.44" x2="15.24" y2="228.6" width="0.1524" layer="91"/>
<wire x1="15.24" y1="228.6" x2="12.7" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="PWR" pin="-"/>
<wire x1="104.14" y1="223.52" x2="104.14" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="109.22" y1="226.06" x2="109.22" y2="220.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="220.98" x2="104.14" y2="220.98" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="104.14" y1="218.44" x2="104.14" y2="220.98" width="0.1524" layer="91"/>
<junction x="104.14" y="220.98"/>
</segment>
<segment>
<pinref part="IC3" gate="PWR" pin="-"/>
<wire x1="119.38" y1="223.52" x2="119.38" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="P$1"/>
<wire x1="124.46" y1="226.06" x2="124.46" y2="220.98" width="0.1524" layer="91"/>
<wire x1="124.46" y1="220.98" x2="119.38" y2="220.98" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="119.38" y1="220.98" x2="119.38" y2="218.44" width="0.1524" layer="91"/>
<junction x="119.38" y="220.98"/>
</segment>
<segment>
<pinref part="IC4" gate="PWR" pin="-"/>
<wire x1="134.62" y1="223.52" x2="134.62" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="P$1"/>
<wire x1="139.7" y1="226.06" x2="139.7" y2="220.98" width="0.1524" layer="91"/>
<wire x1="139.7" y1="220.98" x2="134.62" y2="220.98" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="134.62" y1="220.98" x2="134.62" y2="218.44" width="0.1524" layer="91"/>
<junction x="134.62" y="220.98"/>
</segment>
<segment>
<pinref part="IC5" gate="PWR" pin="-"/>
<wire x1="149.86" y1="223.52" x2="149.86" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="P$1"/>
<wire x1="154.94" y1="226.06" x2="154.94" y2="220.98" width="0.1524" layer="91"/>
<wire x1="154.94" y1="220.98" x2="149.86" y2="220.98" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="149.86" y1="220.98" x2="149.86" y2="218.44" width="0.1524" layer="91"/>
<junction x="149.86" y="220.98"/>
</segment>
<segment>
<pinref part="PWR" gate="G$1" pin="C"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="223.52" y1="223.52" x2="223.52" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="PWR" pin="-"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="165.1" y1="218.44" x2="165.1" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERVO1" gate="G$1" pin="P$1"/>
<wire x1="198.12" y1="58.42" x2="200.66" y2="58.42" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="200.66" y1="58.42" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="SERVO2" gate="G$1" pin="P$1"/>
<wire x1="241.3" y1="55.88" x2="241.3" y2="58.42" width="0.1524" layer="91"/>
<wire x1="241.3" y1="58.42" x2="238.76" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="SERVO3" gate="G$1" pin="P$1"/>
<wire x1="281.94" y1="55.88" x2="281.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="281.94" y1="58.42" x2="279.4" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="SERVO0" gate="G$1" pin="P$1"/>
<wire x1="160.02" y1="55.88" x2="160.02" y2="58.42" width="0.1524" layer="91"/>
<wire x1="160.02" y1="58.42" x2="157.48" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="P$1"/>
<wire x1="185.42" y1="226.06" x2="185.42" y2="220.98" width="0.1524" layer="91"/>
<wire x1="185.42" y1="220.98" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
<pinref part="IC6" gate="PWR" pin="-"/>
<wire x1="180.34" y1="220.98" x2="180.34" y2="223.52" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="180.34" y1="218.44" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
<junction x="180.34" y="220.98"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="SERVO4" gate="G$1" pin="P$1"/>
<wire x1="322.58" y1="55.88" x2="322.58" y2="58.42" width="0.1524" layer="91"/>
<wire x1="322.58" y1="58.42" x2="320.04" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="RF-UART" gate="G$1" pin="P$2"/>
<wire x1="83.82" y1="10.16" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<wire x1="83.82" y1="15.24" x2="86.36" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="PWROUT" pin="GND"/>
<wire x1="38.1" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="40.64" y1="114.3" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM0" gate="G$1" pin="P$1"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="157.48" y1="109.22" x2="152.4" y2="109.22" width="0.1524" layer="91"/>
<wire x1="152.4" y1="109.22" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM1" gate="G$1" pin="P$1"/>
<wire x1="172.72" y1="109.22" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="167.64" y1="109.22" x2="167.64" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM2" gate="G$1" pin="P$1"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="187.96" y1="109.22" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
<wire x1="182.88" y1="109.22" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM3" gate="G$1" pin="P$1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="200.66" y1="109.22" x2="195.58" y2="109.22" width="0.1524" layer="91"/>
<wire x1="195.58" y1="109.22" x2="195.58" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM4" gate="G$1" pin="P$1"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="213.36" y1="109.22" x2="208.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="208.28" y1="109.22" x2="208.28" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM5" gate="G$1" pin="P$1"/>
<wire x1="226.06" y1="109.22" x2="220.98" y2="109.22" width="0.1524" layer="91"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="220.98" y1="109.22" x2="220.98" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM6" gate="G$1" pin="P$1"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="238.76" y1="109.22" x2="233.68" y2="109.22" width="0.1524" layer="91"/>
<wire x1="233.68" y1="109.22" x2="233.68" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM7" gate="G$1" pin="P$1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="251.46" y1="109.22" x2="246.38" y2="109.22" width="0.1524" layer="91"/>
<wire x1="246.38" y1="109.22" x2="246.38" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM8" gate="G$1" pin="P$1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="266.7" y1="109.22" x2="261.62" y2="109.22" width="0.1524" layer="91"/>
<wire x1="261.62" y1="109.22" x2="261.62" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM9" gate="G$1" pin="P$1"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="281.94" y1="109.22" x2="276.86" y2="109.22" width="0.1524" layer="91"/>
<wire x1="276.86" y1="109.22" x2="276.86" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM10" gate="G$1" pin="P$1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="299.72" y1="109.22" x2="294.64" y2="109.22" width="0.1524" layer="91"/>
<wire x1="294.64" y1="109.22" x2="294.64" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="IC7" gate="DPWR" pin="-"/>
<wire x1="50.8" y1="104.14" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="50.8" y1="106.68" x2="50.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="55.88" y1="111.76" x2="55.88" y2="106.68" width="0.1524" layer="91"/>
<wire x1="55.88" y1="106.68" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<junction x="50.8" y="106.68"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="P$1"/>
<wire x1="200.66" y1="226.06" x2="200.66" y2="220.98" width="0.1524" layer="91"/>
<wire x1="200.66" y1="220.98" x2="195.58" y2="220.98" width="0.1524" layer="91"/>
<pinref part="IC7" gate="APWR" pin="-"/>
<wire x1="195.58" y1="220.98" x2="195.58" y2="223.52" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="195.58" y1="218.44" x2="195.58" y2="220.98" width="0.1524" layer="91"/>
<junction x="195.58" y="220.98"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="30.48" y1="231.14" x2="22.86" y2="231.14" width="0.1524" layer="91"/>
<wire x1="22.86" y1="231.14" x2="22.86" y2="218.44" width="0.1524" layer="91"/>
<pinref part="VREG1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="30.48" y1="180.34" x2="33.02" y2="180.34" width="0.1524" layer="91"/>
<wire x1="33.02" y1="180.34" x2="33.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="33.02" y1="172.72" x2="25.4" y2="172.72" width="0.1524" layer="91"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="25.4" y1="172.72" x2="25.4" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="25.4" y1="172.72" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<junction x="25.4" y="172.72"/>
</segment>
<segment>
<pinref part="GND32" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="C"/>
<wire x1="53.34" y1="218.44" x2="53.34" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="C"/>
<wire x1="60.96" y1="218.44" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND34" gate="1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="C"/>
<wire x1="68.58" y1="218.44" x2="68.58" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="C"/>
<wire x1="76.2" y1="218.44" x2="76.2" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$2"/>
<pinref part="PWR" gate="G$1" pin="A"/>
<wire x1="223.52" y1="231.14" x2="223.52" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="223.52" y1="246.38" x2="223.52" y2="243.84" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="+"/>
<wire x1="223.52" y1="243.84" x2="223.52" y2="241.3" width="0.1524" layer="91"/>
<wire x1="88.9" y1="238.76" x2="88.9" y2="241.3" width="0.1524" layer="91"/>
<wire x1="88.9" y1="241.3" x2="88.9" y2="243.84" width="0.1524" layer="91"/>
<wire x1="88.9" y1="243.84" x2="104.14" y2="243.84" width="0.1524" layer="91"/>
<wire x1="104.14" y1="243.84" x2="119.38" y2="243.84" width="0.1524" layer="91"/>
<wire x1="119.38" y1="243.84" x2="134.62" y2="243.84" width="0.1524" layer="91"/>
<wire x1="134.62" y1="243.84" x2="149.86" y2="243.84" width="0.1524" layer="91"/>
<wire x1="149.86" y1="243.84" x2="165.1" y2="243.84" width="0.1524" layer="91"/>
<wire x1="165.1" y1="243.84" x2="180.34" y2="243.84" width="0.1524" layer="91"/>
<pinref part="IC2" gate="PWR" pin="+"/>
<wire x1="104.14" y1="238.76" x2="104.14" y2="241.3" width="0.1524" layer="91"/>
<junction x="104.14" y="243.84"/>
<pinref part="IC3" gate="PWR" pin="+"/>
<wire x1="104.14" y1="241.3" x2="104.14" y2="243.84" width="0.1524" layer="91"/>
<wire x1="119.38" y1="238.76" x2="119.38" y2="241.3" width="0.1524" layer="91"/>
<junction x="119.38" y="243.84"/>
<pinref part="IC4" gate="PWR" pin="+"/>
<wire x1="119.38" y1="241.3" x2="119.38" y2="243.84" width="0.1524" layer="91"/>
<wire x1="134.62" y1="238.76" x2="134.62" y2="241.3" width="0.1524" layer="91"/>
<junction x="134.62" y="243.84"/>
<pinref part="IC5" gate="PWR" pin="+"/>
<wire x1="134.62" y1="241.3" x2="134.62" y2="243.84" width="0.1524" layer="91"/>
<wire x1="149.86" y1="238.76" x2="149.86" y2="241.3" width="0.1524" layer="91"/>
<junction x="149.86" y="243.84"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
<wire x1="149.86" y1="241.3" x2="149.86" y2="243.84" width="0.1524" layer="91"/>
<wire x1="88.9" y1="241.3" x2="93.98" y2="241.3" width="0.1524" layer="91"/>
<wire x1="93.98" y1="241.3" x2="93.98" y2="236.22" width="0.1524" layer="91"/>
<junction x="88.9" y="241.3"/>
<wire x1="104.14" y1="241.3" x2="109.22" y2="241.3" width="0.1524" layer="91"/>
<junction x="104.14" y="241.3"/>
<pinref part="C2" gate="G$1" pin="P$2"/>
<wire x1="109.22" y1="241.3" x2="109.22" y2="236.22" width="0.1524" layer="91"/>
<wire x1="119.38" y1="241.3" x2="124.46" y2="241.3" width="0.1524" layer="91"/>
<junction x="119.38" y="241.3"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<wire x1="124.46" y1="241.3" x2="124.46" y2="236.22" width="0.1524" layer="91"/>
<wire x1="134.62" y1="241.3" x2="139.7" y2="241.3" width="0.1524" layer="91"/>
<junction x="134.62" y="241.3"/>
<pinref part="C4" gate="G$1" pin="P$2"/>
<wire x1="139.7" y1="241.3" x2="139.7" y2="236.22" width="0.1524" layer="91"/>
<wire x1="149.86" y1="241.3" x2="154.94" y2="241.3" width="0.1524" layer="91"/>
<junction x="149.86" y="241.3"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="154.94" y1="241.3" x2="154.94" y2="236.22" width="0.1524" layer="91"/>
<pinref part="PI1" gate="PWR" pin="+"/>
<junction x="165.1" y="243.84"/>
<wire x1="165.1" y1="238.76" x2="165.1" y2="243.84" width="0.1524" layer="91"/>
<pinref part="IC6" gate="PWR" pin="+"/>
<wire x1="180.34" y1="238.76" x2="180.34" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="P$2"/>
<wire x1="180.34" y1="241.3" x2="180.34" y2="243.84" width="0.1524" layer="91"/>
<wire x1="185.42" y1="236.22" x2="185.42" y2="241.3" width="0.1524" layer="91"/>
<wire x1="185.42" y1="241.3" x2="180.34" y2="241.3" width="0.1524" layer="91"/>
<junction x="180.34" y="241.3"/>
<pinref part="C8" gate="G$1" pin="P$2"/>
<wire x1="200.66" y1="243.84" x2="200.66" y2="236.22" width="0.1524" layer="91"/>
<wire x1="195.58" y1="243.84" x2="200.66" y2="243.84" width="0.1524" layer="91"/>
<pinref part="IC7" gate="APWR" pin="+"/>
<wire x1="195.58" y1="238.76" x2="195.58" y2="243.84" width="0.1524" layer="91"/>
<wire x1="180.34" y1="243.84" x2="195.58" y2="243.84" width="0.1524" layer="91"/>
<junction x="180.34" y="243.84"/>
<junction x="195.58" y="243.84"/>
<wire x1="200.66" y1="243.84" x2="223.52" y2="243.84" width="0.1524" layer="91"/>
<junction x="200.66" y="243.84"/>
<junction x="223.52" y="243.84"/>
<wire x1="30.48" y1="228.6" x2="25.4" y2="228.6" width="0.1524" layer="91"/>
<wire x1="25.4" y1="228.6" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<wire x1="25.4" y1="220.98" x2="45.72" y2="220.98" width="0.1524" layer="91"/>
<wire x1="45.72" y1="220.98" x2="45.72" y2="243.84" width="0.1524" layer="91"/>
<wire x1="45.72" y1="243.84" x2="53.34" y2="243.84" width="0.1524" layer="91"/>
<junction x="88.9" y="243.84"/>
<pinref part="R4" gate="G$1" pin="P$2"/>
<wire x1="53.34" y1="243.84" x2="60.96" y2="243.84" width="0.1524" layer="91"/>
<wire x1="60.96" y1="243.84" x2="68.58" y2="243.84" width="0.1524" layer="91"/>
<wire x1="68.58" y1="243.84" x2="76.2" y2="243.84" width="0.1524" layer="91"/>
<wire x1="76.2" y1="243.84" x2="88.9" y2="243.84" width="0.1524" layer="91"/>
<wire x1="40.64" y1="243.84" x2="45.72" y2="243.84" width="0.1524" layer="91"/>
<junction x="45.72" y="243.84"/>
<pinref part="C10" gate="G$1" pin="A"/>
<wire x1="76.2" y1="236.22" x2="76.2" y2="243.84" width="0.1524" layer="91"/>
<junction x="76.2" y="243.84"/>
<pinref part="C13" gate="G$1" pin="A"/>
<wire x1="68.58" y1="236.22" x2="68.58" y2="243.84" width="0.1524" layer="91"/>
<junction x="68.58" y="243.84"/>
<pinref part="C12" gate="G$1" pin="A"/>
<wire x1="60.96" y1="236.22" x2="60.96" y2="243.84" width="0.1524" layer="91"/>
<junction x="60.96" y="243.84"/>
<pinref part="C11" gate="G$1" pin="A"/>
<wire x1="53.34" y1="236.22" x2="53.34" y2="243.84" width="0.1524" layer="91"/>
<junction x="53.34" y="243.84"/>
<pinref part="R3" gate="G$1" pin="P$2"/>
<wire x1="25.4" y1="213.36" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<junction x="25.4" y="220.98"/>
<pinref part="VREG1" gate="G$1" pin="VOUT"/>
</segment>
<segment>
<pinref part="P+6" gate="1" pin="+5V"/>
<wire x1="327.66" y1="78.74" x2="327.66" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="IP+2"/>
<wire x1="327.66" y1="71.12" x2="327.66" y2="68.58" width="0.1524" layer="91"/>
<wire x1="327.66" y1="68.58" x2="330.2" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="IP+1"/>
<wire x1="330.2" y1="71.12" x2="327.66" y2="71.12" width="0.1524" layer="91"/>
<junction x="327.66" y="71.12"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="IP+2"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="289.56" y1="68.58" x2="287.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="287.02" y1="68.58" x2="287.02" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="IP+1"/>
<wire x1="287.02" y1="71.12" x2="287.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="289.56" y1="71.12" x2="287.02" y2="71.12" width="0.1524" layer="91"/>
<junction x="287.02" y="71.12"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="IP+2"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="248.92" y1="68.58" x2="246.38" y2="68.58" width="0.1524" layer="91"/>
<wire x1="246.38" y1="68.58" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="IP+1"/>
<wire x1="246.38" y1="71.12" x2="246.38" y2="78.74" width="0.1524" layer="91"/>
<wire x1="248.92" y1="71.12" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
<junction x="246.38" y="71.12"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="IP+2"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="208.28" y1="68.58" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<wire x1="205.74" y1="68.58" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="IP+1"/>
<wire x1="205.74" y1="71.12" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="71.12" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<junction x="205.74" y="71.12"/>
</segment>
<segment>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="165.1" y1="78.74" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="IP+2"/>
<wire x1="165.1" y1="71.12" x2="165.1" y2="68.58" width="0.1524" layer="91"/>
<wire x1="165.1" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="IP+1"/>
<wire x1="167.64" y1="71.12" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<junction x="165.1" y="71.12"/>
</segment>
<segment>
<pinref part="PWM9" gate="G$1" pin="P$2"/>
<pinref part="P+16" gate="1" pin="+5V"/>
<wire x1="281.94" y1="106.68" x2="279.4" y2="106.68" width="0.1524" layer="91"/>
<wire x1="279.4" y1="106.68" x2="279.4" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM8" gate="G$1" pin="P$2"/>
<pinref part="P+15" gate="1" pin="+5V"/>
<wire x1="266.7" y1="106.68" x2="264.16" y2="106.68" width="0.1524" layer="91"/>
<wire x1="264.16" y1="106.68" x2="264.16" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM7" gate="G$1" pin="P$2"/>
<pinref part="P+14" gate="1" pin="+5V"/>
<wire x1="251.46" y1="106.68" x2="248.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="248.92" y1="106.68" x2="248.92" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM6" gate="G$1" pin="P$2"/>
<pinref part="P+13" gate="1" pin="+5V"/>
<wire x1="238.76" y1="106.68" x2="236.22" y2="106.68" width="0.1524" layer="91"/>
<wire x1="236.22" y1="106.68" x2="236.22" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM5" gate="G$1" pin="P$2"/>
<pinref part="P+12" gate="1" pin="+5V"/>
<wire x1="226.06" y1="106.68" x2="223.52" y2="106.68" width="0.1524" layer="91"/>
<wire x1="223.52" y1="106.68" x2="223.52" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM4" gate="G$1" pin="P$2"/>
<pinref part="P+11" gate="1" pin="+5V"/>
<wire x1="213.36" y1="106.68" x2="210.82" y2="106.68" width="0.1524" layer="91"/>
<wire x1="210.82" y1="106.68" x2="210.82" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM3" gate="G$1" pin="P$2"/>
<pinref part="P+10" gate="1" pin="+5V"/>
<wire x1="200.66" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<wire x1="198.12" y1="106.68" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+9" gate="1" pin="+5V"/>
<pinref part="PWM2" gate="G$1" pin="P$2"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="106.68" width="0.1524" layer="91"/>
<wire x1="185.42" y1="106.68" x2="187.96" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+5V"/>
<pinref part="PWM1" gate="G$1" pin="P$2"/>
<wire x1="170.18" y1="111.76" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="170.18" y1="106.68" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="1" pin="+5V"/>
<pinref part="PWM0" gate="G$1" pin="P$2"/>
<wire x1="154.94" y1="111.76" x2="154.94" y2="106.68" width="0.1524" layer="91"/>
<wire x1="154.94" y1="106.68" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM10" gate="G$1" pin="P$2"/>
<pinref part="P+17" gate="1" pin="+5V"/>
<wire x1="299.72" y1="106.68" x2="297.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="297.18" y1="106.68" x2="297.18" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="VCC" gate="G$1" pin="+"/>
<wire x1="15.24" y1="233.68" x2="12.7" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="IP-1"/>
<wire x1="208.28" y1="63.5" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="205.74" y1="63.5" x2="205.74" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="IP-2"/>
<wire x1="208.28" y1="60.96" x2="205.74" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SERVO1" gate="G$1" pin="P$2"/>
<wire x1="205.74" y1="60.96" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="205.74" y="60.96"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="IP-1"/>
<wire x1="248.92" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="246.38" y1="63.5" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="IP-2"/>
<wire x1="248.92" y1="60.96" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SERVO2" gate="G$1" pin="P$2"/>
<wire x1="246.38" y1="60.96" x2="238.76" y2="60.96" width="0.1524" layer="91"/>
<junction x="246.38" y="60.96"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="IP-1"/>
<wire x1="289.56" y1="63.5" x2="287.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="287.02" y1="63.5" x2="287.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="IP-2"/>
<wire x1="289.56" y1="60.96" x2="287.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SERVO3" gate="G$1" pin="P$2"/>
<wire x1="287.02" y1="60.96" x2="279.4" y2="60.96" width="0.1524" layer="91"/>
<junction x="287.02" y="60.96"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="IP-1"/>
<wire x1="330.2" y1="63.5" x2="327.66" y2="63.5" width="0.1524" layer="91"/>
<wire x1="327.66" y1="63.5" x2="327.66" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="IP-2"/>
<wire x1="330.2" y1="60.96" x2="327.66" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SERVO4" gate="G$1" pin="P$2"/>
<wire x1="327.66" y1="60.96" x2="320.04" y2="60.96" width="0.1524" layer="91"/>
<junction x="327.66" y="60.96"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="IP-1"/>
<wire x1="167.64" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="165.1" y1="63.5" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="IP-2"/>
<wire x1="167.64" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SERVO0" gate="G$1" pin="P$2"/>
<wire x1="165.1" y1="60.96" x2="157.48" y2="60.96" width="0.1524" layer="91"/>
<junction x="165.1" y="60.96"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="PI1" gate="I2C" pin="SCL"/>
<pinref part="IC6" gate="G$2" pin="SCL"/>
<wire x1="38.1" y1="83.82" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<pinref part="HDR4" gate="G$1" pin="P$1"/>
<wire x1="91.44" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="73.66" y1="35.56" x2="71.12" y2="35.56" width="0.1524" layer="91"/>
<wire x1="71.12" y1="35.56" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<wire x1="71.12" y1="33.02" x2="91.44" y2="33.02" width="0.1524" layer="91"/>
<wire x1="91.44" y1="33.02" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<junction x="91.44" y="83.82"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="SDA"/>
<wire x1="106.68" y1="81.28" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<wire x1="93.98" y1="81.28" x2="40.64" y2="81.28" width="0.1524" layer="91"/>
<wire x1="40.64" y1="81.28" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<pinref part="PI1" gate="I2C" pin="SDA"/>
<wire x1="40.64" y1="78.74" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<wire x1="93.98" y1="81.28" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<junction x="93.98" y="81.28"/>
<wire x1="93.98" y1="30.48" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="30.48" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<pinref part="HDR4" gate="G$1" pin="P$2"/>
<wire x1="68.58" y1="38.1" x2="73.66" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="RF-UART" gate="G$1" pin="P$1"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="86.36" y1="12.7" x2="81.28" y2="12.7" width="0.1524" layer="91"/>
<wire x1="81.28" y1="12.7" x2="81.28" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="PI1" gate="PWROUT" pin="+3V3"/>
<wire x1="40.64" y1="129.54" x2="40.64" y2="127" width="0.1524" layer="91"/>
<wire x1="40.64" y1="127" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="40.64" y1="119.38" x2="38.1" y2="119.38" width="0.1524" layer="91"/>
<pinref part="IC7" gate="DPWR" pin="+"/>
<wire x1="50.8" y1="124.46" x2="50.8" y2="127" width="0.1524" layer="91"/>
<wire x1="50.8" y1="127" x2="40.64" y2="127" width="0.1524" layer="91"/>
<junction x="40.64" y="127"/>
<wire x1="50.8" y1="127" x2="55.88" y2="127" width="0.1524" layer="91"/>
<junction x="50.8" y="127"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="55.88" y1="127" x2="55.88" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="A0"/>
<wire x1="104.14" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPI04"/>
<wire x1="38.1" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="A1"/>
<wire x1="104.14" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO5"/>
<wire x1="38.1" y1="71.12" x2="40.64" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="A2"/>
<wire x1="104.14" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO6"/>
<wire x1="38.1" y1="68.58" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="A3"/>
<wire x1="104.14" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO12"/>
<wire x1="38.1" y1="66.04" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="A5"/>
<wire x1="104.14" y1="63.5" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO16"/>
<wire x1="38.1" y1="60.96" x2="40.64" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!OE" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="!OE"/>
<wire x1="104.14" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO17"/>
<wire x1="38.1" y1="58.42" x2="40.64" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO0" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED0"/>
<wire x1="132.08" y1="83.82" x2="134.62" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERVO0" gate="G$1" pin="P$3"/>
<wire x1="157.48" y1="63.5" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="160.02" y1="63.5" x2="160.02" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO1" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED1"/>
<wire x1="132.08" y1="81.28" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERVO1" gate="G$1" pin="P$3"/>
<wire x1="198.12" y1="63.5" x2="200.66" y2="63.5" width="0.1524" layer="91"/>
<wire x1="200.66" y1="63.5" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO2" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED2"/>
<wire x1="132.08" y1="78.74" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SERVO2" gate="G$1" pin="P$3"/>
<wire x1="238.76" y1="63.5" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="241.3" y1="63.5" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO3" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED3"/>
<wire x1="132.08" y1="76.2" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="281.94" y1="83.82" x2="281.94" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SERVO3" gate="G$1" pin="P$3"/>
<wire x1="281.94" y1="63.5" x2="279.4" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="PI1" gate="SPI" pin="MISO"/>
<wire x1="38.1" y1="27.94" x2="40.64" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="MISO"/>
<wire x1="365.76" y1="43.18" x2="363.22" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="HDR1" gate="G$1" pin="P$2"/>
<wire x1="111.76" y1="22.86" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="PI1" gate="SPI" pin="MOSI"/>
<wire x1="38.1" y1="25.4" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="MOSI"/>
<wire x1="365.76" y1="40.64" x2="363.22" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="HDR1" gate="G$1" pin="P$1"/>
<wire x1="114.3" y1="22.86" x2="114.3" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="PI1" gate="SPI" pin="SCLK"/>
<wire x1="38.1" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="SCLK"/>
<wire x1="365.76" y1="45.72" x2="363.22" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="HDR1" gate="G$1" pin="P$3"/>
<wire x1="109.22" y1="22.86" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="PI1" gate="SPI" pin="CE0"/>
<wire x1="38.1" y1="20.32" x2="40.64" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="!CS"/>
<wire x1="365.76" y1="38.1" x2="363.22" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREAD0" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="190.5" y1="45.72" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<wire x1="190.5" y1="68.58" x2="187.96" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="CH0"/>
<wire x1="365.76" y1="68.58" x2="358.14" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREAD1" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VOUT"/>
<wire x1="231.14" y1="45.72" x2="231.14" y2="68.58" width="0.1524" layer="91"/>
<wire x1="231.14" y1="68.58" x2="228.6" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="CH1"/>
<wire x1="365.76" y1="66.04" x2="358.14" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREAD2" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="VOUT"/>
<wire x1="271.78" y1="45.72" x2="271.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="271.78" y1="68.58" x2="269.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="CH2"/>
<wire x1="365.76" y1="63.5" x2="358.14" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREAD3" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VOUT"/>
<wire x1="312.42" y1="45.72" x2="312.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="312.42" y1="68.58" x2="309.88" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="CH3"/>
<wire x1="365.76" y1="60.96" x2="358.14" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREAD4" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="VOUT"/>
<wire x1="350.52" y1="68.58" x2="353.06" y2="68.58" width="0.1524" layer="91"/>
<wire x1="353.06" y1="68.58" x2="353.06" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="CH4"/>
<wire x1="365.76" y1="58.42" x2="358.14" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="PI1" gate="UART" pin="TX"/>
<wire x1="38.1" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<wire x1="73.66" y1="12.7" x2="73.66" y2="20.32" width="0.1524" layer="91"/>
<pinref part="RF-UART" gate="G$1" pin="P$3"/>
<wire x1="73.66" y1="20.32" x2="86.36" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="RF-UART" gate="G$1" pin="P$4"/>
<wire x1="86.36" y1="22.86" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
<wire x1="43.18" y1="22.86" x2="43.18" y2="7.62" width="0.1524" layer="91"/>
<pinref part="PI1" gate="UART" pin="RX"/>
<wire x1="43.18" y1="7.62" x2="38.1" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM0" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED5"/>
<wire x1="134.62" y1="71.12" x2="132.08" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM0" gate="G$1" pin="P$3"/>
<wire x1="154.94" y1="96.52" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="154.94" y1="104.14" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED6"/>
<wire x1="134.62" y1="68.58" x2="132.08" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM1" gate="G$1" pin="P$3"/>
<wire x1="172.72" y1="104.14" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<wire x1="170.18" y1="104.14" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED7"/>
<wire x1="134.62" y1="66.04" x2="132.08" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM2" gate="G$1" pin="P$3"/>
<wire x1="187.96" y1="104.14" x2="185.42" y2="104.14" width="0.1524" layer="91"/>
<wire x1="185.42" y1="104.14" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED8"/>
<wire x1="134.62" y1="63.5" x2="132.08" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM3" gate="G$1" pin="P$3"/>
<wire x1="200.66" y1="104.14" x2="198.12" y2="104.14" width="0.1524" layer="91"/>
<wire x1="198.12" y1="104.14" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED9"/>
<wire x1="134.62" y1="60.96" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM4" gate="G$1" pin="P$3"/>
<wire x1="213.36" y1="104.14" x2="210.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="210.82" y1="104.14" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED10"/>
<wire x1="134.62" y1="58.42" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM5" gate="G$1" pin="P$3"/>
<wire x1="226.06" y1="104.14" x2="223.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="223.52" y1="104.14" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED11"/>
<wire x1="134.62" y1="55.88" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM6" gate="G$1" pin="P$3"/>
<wire x1="238.76" y1="104.14" x2="236.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="236.22" y1="104.14" x2="236.22" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED12"/>
<wire x1="134.62" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM7" gate="G$1" pin="P$3"/>
<wire x1="251.46" y1="104.14" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="248.92" y1="104.14" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED13"/>
<wire x1="134.62" y1="50.8" x2="132.08" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM8" gate="G$1" pin="P$3"/>
<wire x1="266.7" y1="104.14" x2="264.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="264.16" y1="104.14" x2="264.16" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM9" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED14"/>
<wire x1="134.62" y1="48.26" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM9" gate="G$1" pin="P$3"/>
<wire x1="281.94" y1="104.14" x2="279.4" y2="104.14" width="0.1524" layer="91"/>
<wire x1="279.4" y1="104.14" x2="279.4" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM10" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="LED15"/>
<wire x1="134.62" y1="45.72" x2="132.08" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PWM10" gate="G$1" pin="P$3"/>
<wire x1="299.72" y1="104.14" x2="297.18" y2="104.14" width="0.1524" layer="91"/>
<wire x1="297.18" y1="104.14" x2="297.18" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO18"/>
<wire x1="38.1" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="43.18" y1="55.88" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="HDR2" gate="G$1" pin="P$4"/>
<wire x1="43.18" y1="71.12" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="HDR2" gate="G$1" pin="P$3"/>
<wire x1="73.66" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<wire x1="45.72" y1="68.58" x2="45.72" y2="53.34" width="0.1524" layer="91"/>
<pinref part="PI1" gate="GPIO" pin="GPIO19"/>
<wire x1="45.72" y1="53.34" x2="38.1" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO20"/>
<wire x1="38.1" y1="50.8" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="50.8" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<pinref part="HDR2" gate="G$1" pin="P$2"/>
<wire x1="48.26" y1="63.5" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="HDR2" gate="G$1" pin="P$1"/>
<wire x1="73.66" y1="60.96" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="50.8" y1="60.96" x2="50.8" y2="48.26" width="0.1524" layer="91"/>
<pinref part="PI1" gate="GPIO" pin="GPIO21"/>
<wire x1="50.8" y1="48.26" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO22"/>
<wire x1="38.1" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="45.72" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<pinref part="HDR3" gate="G$1" pin="P$4"/>
<wire x1="53.34" y1="58.42" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="HDR3" gate="G$1" pin="P$3"/>
<wire x1="73.66" y1="55.88" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<wire x1="55.88" y1="55.88" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="PI1" gate="GPIO" pin="GPIO23"/>
<wire x1="55.88" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="HDR3" gate="G$1" pin="P$2"/>
<wire x1="73.66" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<pinref part="PI1" gate="GPIO" pin="GPIO24"/>
<wire x1="58.42" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO25"/>
<wire x1="38.1" y1="38.1" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="60.96" y1="38.1" x2="60.96" y2="48.26" width="0.1524" layer="91"/>
<pinref part="HDR3" gate="G$1" pin="P$1"/>
<wire x1="60.96" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO26"/>
<wire x1="38.1" y1="35.56" x2="63.5" y2="35.56" width="0.1524" layer="91"/>
<wire x1="63.5" y1="35.56" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<pinref part="HDR4" gate="G$1" pin="P$4"/>
<wire x1="63.5" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="HDR4" gate="G$1" pin="P$3"/>
<wire x1="73.66" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="66.04" y2="33.02" width="0.1524" layer="91"/>
<pinref part="PI1" gate="GPIO" pin="GPIO27"/>
<wire x1="66.04" y1="33.02" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="20.32" y1="233.68" x2="25.4" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="P$1"/>
<wire x1="25.4" y1="233.68" x2="30.48" y2="233.68" width="0.1524" layer="91"/>
<wire x1="30.48" y1="243.84" x2="25.4" y2="243.84" width="0.1524" layer="91"/>
<wire x1="25.4" y1="243.84" x2="25.4" y2="236.22" width="0.1524" layer="91"/>
<junction x="25.4" y="233.68"/>
<wire x1="25.4" y1="236.22" x2="25.4" y2="233.68" width="0.1524" layer="91"/>
<wire x1="30.48" y1="236.22" x2="25.4" y2="236.22" width="0.1524" layer="91"/>
<junction x="25.4" y="236.22"/>
<pinref part="VREG1" gate="G$1" pin="ON/OFF"/>
<pinref part="VREG1" gate="G$1" pin="VIN"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="3"/>
<pinref part="C9" gate="G$1" pin="P$1"/>
<wire x1="25.4" y1="185.42" x2="25.4" y2="187.96" width="0.1524" layer="91"/>
<wire x1="25.4" y1="187.96" x2="25.4" y2="190.5" width="0.1524" layer="91"/>
<wire x1="30.48" y1="226.06" x2="27.94" y2="226.06" width="0.1524" layer="91"/>
<wire x1="27.94" y1="226.06" x2="27.94" y2="187.96" width="0.1524" layer="91"/>
<wire x1="27.94" y1="187.96" x2="25.4" y2="187.96" width="0.1524" layer="91"/>
<junction x="25.4" y="187.96"/>
<pinref part="VREG1" gate="G$1" pin="TRIM"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="P$2"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="25.4" y1="200.66" x2="25.4" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="IC6" gate="G$2" pin="A4"/>
<wire x1="104.14" y1="66.04" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PI1" gate="GPIO" pin="GPIO13"/>
<wire x1="38.1" y1="63.5" x2="40.64" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO4" class="0">
<segment>
<wire x1="322.58" y1="83.82" x2="322.58" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SERVO4" gate="G$1" pin="P$3"/>
<wire x1="322.58" y1="63.5" x2="320.04" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC6" gate="G$2" pin="LED4"/>
<wire x1="134.62" y1="73.66" x2="132.08" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
