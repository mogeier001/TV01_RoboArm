<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="100" unitdist="mil" unit="mil" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="ReferenceLS" color="12" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="10" fill="10" visible="no" active="no"/>
<layer number="210" name="210bmp" color="11" fill="10" visible="no" active="no"/>
<layer number="211" name="211bmp" color="12" fill="10" visible="no" active="no"/>
<layer number="212" name="212bmp" color="13" fill="10" visible="no" active="no"/>
<layer number="213" name="213bmp" color="14" fill="10" visible="no" active="no"/>
<layer number="214" name="214bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="215" name="215bmp" color="16" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="17" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="RobticArm">
<packages>
<package name="SOP65P640X120-24" urn="urn:adsk.eagle:footprint:7056865/1" locally_modified="yes">
<description>24-SOP, 0.65 mm pitch, 6.40 mm span, 7.80 X 4.40 X 1.20 mm body
&lt;p&gt;24-pin SOP package with 0.65 mm pitch, 6.40 mm span with body size 7.80 X 4.40 X 1.20 mm&lt;/p&gt;</description>
<circle x="-2.9646" y="4.3286" radius="0.25" width="0" layer="21"/>
<smd name="1" x="-2.8741" y="3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="2" x="-2.8741" y="2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="3" x="-2.8741" y="2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="4" x="-2.8741" y="1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="5" x="-2.8741" y="0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="6" x="-2.8741" y="0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="7" x="-2.8741" y="-0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="8" x="-2.8741" y="-0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="9" x="-2.8741" y="-1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="10" x="-2.8741" y="-2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="11" x="-2.8741" y="-2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="12" x="-2.8741" y="-3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="13" x="2.8741" y="-3.575" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="14" x="2.8741" y="-2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="15" x="2.8741" y="-2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="16" x="2.8741" y="-1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="17" x="2.8741" y="-0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="18" x="2.8741" y="-0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="19" x="2.8741" y="0.325" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="20" x="2.8741" y="0.975" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="21" x="2.8741" y="1.625" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="22" x="2.8741" y="2.275" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="23" x="2.8741" y="2.925" dx="1.6101" dy="0.4992" layer="1"/>
<smd name="24" x="2.8741" y="3.575" dx="1.6101" dy="0.4992" layer="1"/>
<text x="0" y="4.479540625" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.7736" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.5875" y1="-3.4925" x2="1.5875" y2="3.4925" layer="1"/>
<wire x1="-1.83515" y1="-4.1290875" x2="1.83515" y2="-4.1290875" width="0.127" layer="21"/>
<wire x1="1.83515" y1="-4.1290875" x2="1.83515" y2="4.1290875" width="0.127" layer="21"/>
<wire x1="1.83515" y1="4.1290875" x2="-1.83515" y2="4.1290875" width="0.127" layer="21"/>
<wire x1="-1.83515" y1="4.1290875" x2="-1.83515" y2="-4.1290875" width="0.127" layer="21"/>
</package>
<package name="CAPC2012X135" urn="urn:adsk.eagle:footprint:7158980/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 1.35 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.35 mm&lt;/p&gt;</description>
<wire x1="0.272125" y1="-0.675" x2="-0.272125" y2="-0.675" width="0.12" layer="21"/>
<wire x1="-0.272125" y1="-0.675" x2="-0.272125" y2="0.675" width="0.12" layer="21"/>
<wire x1="-0.272125" y1="0.675" x2="0.272125" y2="0.675" width="0.12" layer="21"/>
<wire x1="0.272125" y1="0.675" x2="0.272125" y2="-0.675" width="0.12" layer="21"/>
<smd name="1" x="-0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<smd name="2" x="0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<text x="0" y="0.896125" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.896125" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.275271875" y1="-0.642303125" x2="0.275271875" y2="0.642303125" layer="21"/>
</package>
<package name="RESC2016X70" urn="urn:adsk.eagle:footprint:7158856/1" locally_modified="yes">
<description>Chip, 2.00 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<wire x1="0.272125" y1="-0.85" x2="-0.272125" y2="-0.85" width="0.127" layer="21"/>
<wire x1="-0.272125" y1="-0.85" x2="-0.272125" y2="0.85" width="0.127" layer="21"/>
<wire x1="-0.272125" y1="0.85" x2="0.272125" y2="0.85" width="0.127" layer="21"/>
<wire x1="0.272125" y1="0.85" x2="0.272125" y2="-0.85" width="0.127" layer="21"/>
<smd name="1" x="-0.9399" y="0" dx="1.0202" dy="1.8" layer="1"/>
<smd name="2" x="0.9399" y="0" dx="1.0202" dy="1.8" layer="1"/>
<text x="0" y="1.071125" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.071125" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC2012X70" urn="urn:adsk.eagle:footprint:7159536/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<smd name="C" x="-0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<smd name="A" x="0.9399" y="0" dx="1.0202" dy="1.45" layer="1"/>
<text x="0" y="0.896125" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.896125" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="0.275271875" y="0.734059375"/>
<vertex x="-0.275271875" y="0.09175625"/>
<vertex x="-0.275271875" y="0"/>
<vertex x="0.275271875" y="-0.734059375"/>
</polygon>
</package>
<package name="2POL254" urn="urn:adsk.eagle:footprint:9305/1" locally_modified="yes">
<description>&lt;b&gt;PHOENIX CONNECTOR&lt;/b&gt;</description>
<pad name="1" x="-2.54" y="0" drill="1.5" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.5" shape="long" rot="R90"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<rectangle x1="-5.08" y1="3.81" x2="5.08" y2="5.08" layer="21"/>
</package>
<package name="DIOM4226X243" urn="urn:adsk.eagle:footprint:7189003/1" locally_modified="yes">
<description>Molded Body, 4.29 X 2.69 X 2.44 mm body
&lt;p&gt;Molded Body package with body size 4.29 X 2.69 X 2.44 mm&lt;/p&gt;</description>
<smd name="C" x="-1.6761" y="0" dx="2.2846" dy="1.5839" layer="1"/>
<smd name="A" x="1.6777" y="0" dx="2.2814" dy="1.5871" layer="1"/>
<text x="0" y="1.1305" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1305" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-0.36703125" y="0"/>
<vertex x="0.367028125" y="0.734059375"/>
<vertex x="0.36703125" y="0.734059375"/>
<vertex x="0.36703125" y="-0.734059375"/>
<vertex x="-0.367028125" y="0"/>
</polygon>
</package>
<package name="CAPAE660X550" urn="urn:adsk.eagle:footprint:7207783/1" locally_modified="yes">
<description>ECAP (Aluminum Electrolytic Capacitor), 6.60 X 5.50 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 6.60 X 5.50 mm&lt;/p&gt;</description>
<wire x1="-3.4" y1="1.079" x2="-3.4" y2="2.1125" width="0.12" layer="21"/>
<wire x1="-3.4" y1="2.1125" x2="-2.1125" y2="3.4" width="0.12" layer="21"/>
<wire x1="-2.1125" y1="3.4" x2="3.4" y2="3.4" width="0.12" layer="21"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="1.079" width="0.12" layer="21"/>
<wire x1="-3.4" y1="-1.079" x2="-3.4" y2="-2.1125" width="0.12" layer="21"/>
<wire x1="-3.4" y1="-2.1125" x2="-2.1125" y2="-3.4" width="0.12" layer="21"/>
<wire x1="-2.1125" y1="-3.4" x2="3.4" y2="-3.4" width="0.12" layer="21"/>
<wire x1="3.4" y1="-3.4" x2="3.4" y2="-1.079" width="0.12" layer="21"/>
<smd name="A" x="-2.6449" y="0" dx="3.5134" dy="1.65" layer="1"/>
<smd name="C" x="2.6449" y="0" dx="3.5134" dy="1.65" layer="1"/>
<text x="0" y="4.035" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.035" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:footprint:7212743/1">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<circle x="-2.504" y="1.6809" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="1.4309" x2="-2" y2="2" width="0.12" layer="21"/>
<wire x1="-2" y1="2" x2="-1.4309" y2="2" width="0.12" layer="21"/>
<wire x1="2" y1="1.4309" x2="2" y2="2" width="0.12" layer="21"/>
<wire x1="2" y1="2" x2="1.4309" y2="2" width="0.12" layer="21"/>
<wire x1="2" y1="-1.4309" x2="2" y2="-2" width="0.12" layer="21"/>
<wire x1="2" y1="-2" x2="1.4309" y2="-2" width="0.12" layer="21"/>
<wire x1="-2" y1="-1.4309" x2="-2" y2="-2" width="0.12" layer="21"/>
<wire x1="-2" y1="-2" x2="-1.4309" y2="-2" width="0.12" layer="21"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.12" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.12" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.12" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.12" layer="51"/>
<smd name="1" x="-1.9261" y="1" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="2" x="-1.9261" y="0.5" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="3" x="-1.9261" y="0" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="4" x="-1.9261" y="-0.5" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="5" x="-1.9261" y="-1" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="6" x="-1" y="-1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="-0.5" y="-1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0" y="-1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="0.5" y="-1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="1" y="-1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="1.9261" y="-1" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="12" x="1.9261" y="-0.5" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="13" x="1.9261" y="0" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="14" x="1.9261" y="0.5" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="15" x="1.9261" y="1" dx="0.9714" dy="0.3538" layer="1" roundness="100"/>
<smd name="16" x="1" y="1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.5" y="1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="0" y="1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="-0.5" y="1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="-1" y="1.9261" dx="0.9714" dy="0.3538" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="0" y="0" dx="2.75" dy="2.75" layer="1" thermals="no"/>
<text x="0" y="3.0468" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.0468" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV6W64P254_2X3_762X508X838B" urn="urn:adsk.eagle:footprint:7212777/1" locally_modified="yes">
<description>Double-row, 6-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X3), 6-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 5.08 X 8.38 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<circle x="-2.54" y="-3.044" radius="0.25" width="0" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.12" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="2.54" width="0.12" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.12" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.12" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.12" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="2.54" width="0.12" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.12" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.12" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.1051" diameter="1.7051" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="0" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="0" y="1.27" drill="1.1051" diameter="1.7051"/>
<pad name="5" x="2.54" y="-1.27" drill="1.1051" diameter="1.7051"/>
<pad name="6" x="2.54" y="1.27" drill="1.1051" diameter="1.7051"/>
<text x="0" y="3.175" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.929" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV4W64P254_1X4_1016X254X838B" urn="urn:adsk.eagle:footprint:7207461/1" locally_modified="yes">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="-3.81" y="1.774" radius="0.25" width="0" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.12" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.12" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.12" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="-1.27" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="1.27" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="3.81" y="0" drill="1.1051" diameter="1.7051"/>
<text x="-2.54" y="-2.421" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
</package>
<package name="TO450P972X240-3" urn="urn:adsk.eagle:footprint:7212898/1" locally_modified="yes">
<description>3-TO, DPAK, 4.50 mm pitch, 9.72 mm span, 6.50 X 6.10 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.50 mm pitch, 9.72 mm span with body size 6.50 X 6.10 X 2.40 mm&lt;/p&gt;</description>
<circle x="-4.2046" y="3.2755" radius="0.25" width="0" layer="21"/>
<wire x1="4.2275" y1="3.034" x2="4.2275" y2="3.3" width="0.12" layer="21"/>
<wire x1="4.2275" y1="3.3" x2="-1.9725" y2="3.3" width="0.12" layer="21"/>
<wire x1="-1.9725" y1="3.3" x2="-1.9725" y2="-3.3" width="0.12" layer="21"/>
<wire x1="-1.9725" y1="-3.3" x2="4.2275" y2="-3.3" width="0.12" layer="21"/>
<wire x1="4.2275" y1="-3.3" x2="4.2275" y2="-3.034" width="0.12" layer="21"/>
<smd name="IN" x="-4.2046" y="2.25" dx="2.4235" dy="1.0429" layer="1"/>
<smd name="OUT" x="-4.2046" y="-2.25" dx="2.4235" dy="1.0429" layer="1"/>
<smd name="GND" x="2.4725" y="0" dx="5.8876" dy="5.56" layer="1"/>
<text x="0" y="4.1605" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.935" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="XTAL1200X480X450" urn="urn:adsk.eagle:footprint:7212914/1" locally_modified="yes">
<description>Crystal, 12.00 X 4.80 X 4.50 mm body
&lt;p&gt;Crystal package with body size 12.00 X 4.80 X 4.50 mm&lt;/p&gt;</description>
<wire x1="-6" y1="1.1779" x2="-6" y2="2.4" width="0.12" layer="21"/>
<wire x1="-6" y1="2.4" x2="6" y2="2.4" width="0.12" layer="21"/>
<wire x1="6" y1="2.4" x2="6" y2="1.1779" width="0.12" layer="21"/>
<wire x1="-6" y1="-1.1779" x2="-6" y2="-2.4" width="0.12" layer="21"/>
<wire x1="-6" y1="-2.4" x2="6" y2="-2.4" width="0.12" layer="21"/>
<wire x1="6" y1="-2.4" x2="6" y2="-1.1779" width="0.12" layer="21"/>
<smd name="1" x="-4.675" y="0" dx="5.3736" dy="1.8477" layer="1"/>
<smd name="2" x="4.675" y="0" dx="5.3736" dy="1.8477" layer="1"/>
<text x="0" y="3.035" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.035" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TO455P987X240-3" urn="urn:adsk.eagle:footprint:7472989/1" locally_modified="yes">
<description>3-TO, DPAK, 4.55 mm pitch, 9.88 mm span, 6.54 X 6.09 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.55 mm pitch, 9.88 mm span with body size 6.54 X 6.09 X 2.40 mm&lt;/p&gt;</description>
<circle x="-4.3452" y="3.3005" radius="0.25" width="0" layer="21"/>
<wire x1="3.8325" y1="3.0374" x2="3.8325" y2="3.365" width="0.12" layer="21"/>
<wire x1="3.8325" y1="3.365" x2="-2.3875" y2="3.365" width="0.12" layer="21"/>
<wire x1="-2.3875" y1="3.365" x2="-2.3875" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-2.3875" y1="-3.365" x2="3.8325" y2="-3.365" width="0.12" layer="21"/>
<wire x1="3.8325" y1="-3.365" x2="3.8325" y2="-3.0374" width="0.12" layer="21"/>
<smd name="A" x="-4.3452" y="2.275" dx="2.4332" dy="1.0429" layer="1"/>
<smd name="A1" x="-4.3452" y="-2.275" dx="2.4332" dy="1.0429" layer="1"/>
<smd name="C" x="2.4182" y="0" dx="6.2871" dy="5.5669" layer="1"/>
<text x="0" y="4.1855" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOP65P640X120-24" urn="urn:adsk.eagle:package:7056855/1" locally_modified="yes" type="model">
<description>24-SOP, 0.65 mm pitch, 6.40 mm span, 7.80 X 4.40 X 1.20 mm body
&lt;p&gt;24-pin SOP package with 0.65 mm pitch, 6.40 mm span with body size 7.80 X 4.40 X 1.20 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP65P640X120-24"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X135" urn="urn:adsk.eagle:package:7158867/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.25 X 1.35 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X135"/>
</packageinstances>
</package3d>
<package3d name="RESC2016X70" urn="urn:adsk.eagle:package:7158853/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2016X70"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X70" urn="urn:adsk.eagle:package:7159533/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X70"/>
</packageinstances>
</package3d>
<package3d name="2POL254" urn="urn:adsk.eagle:package:9320/1" locally_modified="yes" type="box">
<description>PHOENIX CONNECTOR</description>
<packageinstances>
<packageinstance name="2POL254"/>
</packageinstances>
</package3d>
<package3d name="DIOM4226X243" urn="urn:adsk.eagle:package:7188975/1" locally_modified="yes" type="model">
<description>Molded Body, 4.29 X 2.69 X 2.44 mm body
&lt;p&gt;Molded Body package with body size 4.29 X 2.69 X 2.44 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM4226X243"/>
</packageinstances>
</package3d>
<package3d name="CAPAE660X550" urn="urn:adsk.eagle:package:7207779/1" locally_modified="yes" type="model">
<description>ECAP (Aluminum Electrolytic Capacitor), 6.60 X 5.50 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 6.60 X 5.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPAE660X550"/>
</packageinstances>
</package3d>
<package3d name="QFN50P400X400X80-21T275" urn="urn:adsk.eagle:package:7212738/1" type="model">
<description>20-QFN, 0.50 mm pitch, 4.00 X 4.00 X 0.80 mm body, 2.75 X 2.75 mm thermal pad
&lt;p&gt;20-pin QFN package with 0.50 mm pitch with body size 4.00 X 4.00 X 0.80 mm and thermal pad size 2.75 X 2.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFN50P400X400X80-21T275"/>
</packageinstances>
</package3d>
<package3d name="HDRV6W64P254_2X3_762X508X838B" urn="urn:adsk.eagle:package:7212775/1" locally_modified="yes" type="model">
<description>Double-row, 6-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X3), 6-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 5.08 X 8.38 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV6W64P254_2X3_762X508X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W64P254_1X4_1016X254X838B" urn="urn:adsk.eagle:package:7207460/1" locally_modified="yes" type="model">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X254X838B"/>
</packageinstances>
</package3d>
<package3d name="TO450P972X240-3" urn="urn:adsk.eagle:package:7212818/1" locally_modified="yes" type="model">
<description>3-TO, DPAK, 4.50 mm pitch, 9.72 mm span, 6.50 X 6.10 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.50 mm pitch, 9.72 mm span with body size 6.50 X 6.10 X 2.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO450P972X240-3"/>
</packageinstances>
</package3d>
<package3d name="XTAL1200X480X450" urn="urn:adsk.eagle:package:7212909/1" locally_modified="yes" type="model">
<description>Crystal, 12.00 X 4.80 X 4.50 mm body
&lt;p&gt;Crystal package with body size 12.00 X 4.80 X 4.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="XTAL1200X480X450"/>
</packageinstances>
</package3d>
<package3d name="TO455P987X240-3" urn="urn:adsk.eagle:package:7472987/1" locally_modified="yes" type="model">
<description>3-TO, DPAK, 4.55 mm pitch, 9.88 mm span, 6.54 X 6.09 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.55 mm pitch, 9.88 mm span with body size 6.54 X 6.09 X 2.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO455P987X240-3"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="GEIER-FRAME_A3">
<wire x1="289.56" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="343.535" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="356.87" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="343.281" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="343.535" y="20.32" size="2.54" layer="94" font="vector">Geier Moritz</text>
<text x="343.535" y="10.16" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
</symbol>
<symbol name="GEIER-DRV8886AT">
<pin name="STP" x="-10.16" y="10.16" visible="pin" length="short" direction="in"/>
<pin name="DIR" x="-10.16" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="DCY" x="-10.16" y="-7.62" visible="pin" length="short" direction="in"/>
<pin name="Q1+" x="10.16" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="Q1-" x="10.16" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="Q2-" x="10.16" y="0" visible="pin" length="short" rot="R180"/>
<pin name="Q2+" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="!FLT" x="10.16" y="-5.08" visible="pin" length="short" rot="R180"/>
<wire x1="-7.62" y1="11.43" x2="-7.62" y2="-16.51" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-16.51" x2="7.62" y2="-16.51" width="0.254" layer="94"/>
<wire x1="7.62" y1="-16.51" x2="7.62" y2="11.43" width="0.254" layer="94"/>
<wire x1="7.62" y1="11.43" x2="-7.62" y2="11.43" width="0.254" layer="94"/>
<text x="-7.62" y="12.7" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="!SLP" x="-10.16" y="-12.7" visible="pin" length="short" direction="in"/>
<pin name="TRQ" x="-10.16" y="-15.24" visible="pin" length="short" direction="in"/>
<pin name="M0" x="-10.16" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="M1" x="-10.16" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="ENB" x="-10.16" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="CPH" x="10.16" y="-10.16" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="CPL" x="10.16" y="-12.7" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="REF" x="-10.16" y="2.54" visible="pin" length="short" direction="in"/>
<text x="-7.62" y="-17.78" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-PWR">
<pin name="+" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="-" x="0" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<wire x1="0" y1="5.133975" x2="0" y2="3.7338" width="0.1524" layer="94"/>
<circle x="0" y="2.644775" radius="0.996165625" width="0.254" layer="94"/>
<wire x1="0" y1="-5.133975" x2="0" y2="-3.7338" width="0.1524" layer="94"/>
<circle x="0" y="-2.644775" radius="0.946325" width="0.254" layer="94"/>
<wire x1="-2.17805" y1="8.0899" x2="-2.17805" y2="7.15645" width="0.1524" layer="97"/>
<wire x1="-2.644775" y1="7.623175" x2="-1.711325" y2="7.623175" width="0.1524" layer="97"/>
<text x="-1.5875" y="-0.9525" size="0.8128" layer="95" font="vector" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="GEIER-DRV886AT_PWR">
<pin name="AVDD" x="2.54" y="0" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="DVDD" x="2.54" y="2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VCP" x="2.54" y="-2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<wire x1="0" y1="3.81" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="-8.89" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-3.81" x2="-8.89" y2="3.81" width="0.254" layer="94"/>
<wire x1="-8.89" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<text x="-8.89" y="5.08" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-C">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-0.635" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="-1.905" width="0.4064" layer="94"/>
<wire x1="0.635" y1="1.905" x2="0.635" y2="-1.905" width="0.4064" layer="94"/>
<text x="0" y="2.54" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-3.81" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-R">
<wire x1="-2.54" y1="0.9525" x2="-2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.9525" x2="2.54" y2="-0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="2.54" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="-2.54" y2="0.9525" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="1.27" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.2225" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-D_LED">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="1.7526" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="-2.6924" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.00933125" y1="1.00933125" x2="0.367034375" y2="1.651634375" width="0.127" layer="94"/>
<wire x1="0.642303125" y1="0.6423" x2="0" y2="1.284603125" width="0.127" layer="94"/>
<polygon width="0.127" layer="94">
<vertex x="0" y="1.28460625"/>
<vertex x="0" y="1.101090625"/>
<vertex x="0.183515625" y="1.28460625"/>
</polygon>
<polygon width="0.127" layer="94">
<vertex x="0.36703125" y="1.651634375"/>
<vertex x="0.36703125" y="1.46811875"/>
<vertex x="0.55054375" y="1.65163125"/>
<vertex x="0.55054375" y="1.651634375"/>
</polygon>
</symbol>
<symbol name="GEIER-X2">
<wire x1="-2.54" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<pin name="+" x="5.08" y="2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="-" x="5.08" y="-2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<text x="-2.54" y="4.445" size="0.8128" layer="95" font="vector">&gt;NAME</text>
</symbol>
<symbol name="GEIER-D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="1.7526" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="-2.6924" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="GEIER-C_POL">
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.905" width="0.4064" layer="94"/>
<wire x1="0.635" y1="-1.905" x2="0.635" y2="0" width="0.4064" layer="94"/>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="in"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="out" rot="R180"/>
<wire x1="0.635" y1="0" x2="0.635" y2="1.905" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-2.921" y1="2.032" x2="-1.905" y2="2.032" width="0.1524" layer="94"/>
<text x="-1.27" y="2.54" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="0.8128" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-ATTINY23">
<pin name="PB0" x="10.16" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="PB1" x="10.16" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="PB2" x="10.16" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="PB3" x="10.16" y="0" visible="pin" length="short" rot="R180"/>
<pin name="PB4" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="PB5" x="10.16" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="PB6" x="10.16" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="PB7" x="10.16" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="PD0" x="-10.16" y="7.62" visible="pin" length="short"/>
<pin name="PD1" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="PD2" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="PD3" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="PD4" x="-10.16" y="-7.62" visible="pin" length="short"/>
<pin name="PD5" x="-10.16" y="-10.16" visible="pin" length="short"/>
<pin name="PD6" x="10.16" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="PA0" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="PA1" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="PA2" x="-10.16" y="10.16" visible="pin" length="short"/>
<wire x1="-7.62" y1="-11.43" x2="-7.62" y2="11.43" width="0.254" layer="94"/>
<wire x1="-7.62" y1="11.43" x2="7.62" y2="11.43" width="0.254" layer="94"/>
<wire x1="7.62" y1="11.43" x2="7.62" y2="-11.43" width="0.254" layer="94"/>
<wire x1="7.62" y1="-11.43" x2="-7.62" y2="-11.43" width="0.254" layer="94"/>
<text x="-7.62" y="12.7" size="0.8128" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-ISP">
<pin name="RESET" x="-5.08" y="7.62" visible="pin" length="short"/>
<pin name="VCC" x="-5.08" y="5.08" visible="pin" length="short"/>
<pin name="SCK" x="-5.08" y="2.54" visible="pin" length="short"/>
<pin name="MISO" x="-5.08" y="-2.54" visible="pin" length="short"/>
<pin name="MOSI" x="-5.08" y="-5.08" visible="pin" length="short"/>
<pin name="GND" x="-5.08" y="-7.62" visible="pin" length="short"/>
<wire x1="-2.54" y1="8.89" x2="-2.54" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-8.89" x2="7.62" y2="-8.89" width="0.254" layer="94"/>
<wire x1="7.62" y1="-8.89" x2="7.62" y2="8.89" width="0.254" layer="94"/>
<wire x1="7.62" y1="8.89" x2="-2.54" y2="8.89" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="0.8128" layer="95">&gt;NAME</text>
</symbol>
<symbol name="GEIER-HDR4">
<pin name="P$1" x="-5.08" y="2.54" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="-2.54" y="2.54" visible="off" length="short" rot="R270"/>
<pin name="P$3" x="2.54" y="2.54" visible="off" length="short" rot="R270"/>
<wire x1="-5.715" y1="0" x2="5.715" y2="0" width="0.254" layer="94"/>
<text x="-5.715" y="-1.27" size="0.8128" layer="95" font="vector">&gt;NAME</text>
<pin name="P$4" x="5.08" y="2.54" visible="off" length="short" rot="R270"/>
</symbol>
<symbol name="GEIER-LM78XX">
<pin name="IN" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="OUT" x="10.16" y="0" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="0" y="-10.16" visible="pin" length="short" rot="R90"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="0.8128" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GEIER-Y">
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.635" x2="2.54" y2="0.635" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="0" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.27" y="2.54" size="0.8128" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.175" size="0.8128" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GEIER-FRAME_A3">
<gates>
<gate name="G$1" symbol="GEIER-FRAME_A3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-DRV8886AT" prefix="IC">
<gates>
<gate name="G$1" symbol="GEIER-DRV8886AT" x="0" y="0"/>
<gate name="PWR" symbol="GEIER-PWR" x="-27.94" y="0" addlevel="request"/>
<gate name="VREG" symbol="GEIER-DRV886AT_PWR" x="2.54" y="20.32"/>
</gates>
<devices>
<device name="HTSSOP" package="SOP65P640X120-24">
<connects>
<connect gate="G$1" pin="!FLT" pad="15"/>
<connect gate="G$1" pin="!SLP" pad="17"/>
<connect gate="G$1" pin="CPH" pad="2"/>
<connect gate="G$1" pin="CPL" pad="1"/>
<connect gate="G$1" pin="DCY" pad="24"/>
<connect gate="G$1" pin="DIR" pad="20"/>
<connect gate="G$1" pin="ENB" pad="18"/>
<connect gate="G$1" pin="M0" pad="21"/>
<connect gate="G$1" pin="M1" pad="22"/>
<connect gate="G$1" pin="Q1+" pad="5"/>
<connect gate="G$1" pin="Q1-" pad="7"/>
<connect gate="G$1" pin="Q2+" pad="8"/>
<connect gate="G$1" pin="Q2-" pad="10"/>
<connect gate="G$1" pin="REF" pad="16"/>
<connect gate="G$1" pin="STP" pad="19"/>
<connect gate="G$1" pin="TRQ" pad="23"/>
<connect gate="PWR" pin="+" pad="4 11"/>
<connect gate="PWR" pin="-" pad="6 9 12"/>
<connect gate="VREG" pin="AVDD" pad="13"/>
<connect gate="VREG" pin="DVDD" pad="14"/>
<connect gate="VREG" pin="VCP" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7056855/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-C" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="CAPC2012X135">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7158867/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-R" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="RESC2016X70">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7158853/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-LED" prefix="LED" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-D_LED" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="LEDC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7159533/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-X2" prefix="X">
<gates>
<gate name="G$1" symbol="GEIER-X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2POL254">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9320/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-D" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-D" x="0" y="0"/>
</gates>
<devices>
<device name="1N4007" package="DIOM4226X243">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7188975/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FRED" package="TO455P987X240-3">
<connects>
<connect gate="G$1" pin="A" pad="A A1"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7472987/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-C_POL" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-C_POL" x="0" y="0"/>
</gates>
<devices>
<device name="6.6MM" package="CAPAE660X550">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7207779/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-ATTINY23" prefix="MCU">
<gates>
<gate name="PWR" symbol="GEIER-PWR" x="-25.4" y="0" addlevel="request"/>
<gate name="G$1" symbol="GEIER-ATTINY23" x="0" y="0"/>
</gates>
<devices>
<device name="QFN" package="QFN50P400X400X80-21T275">
<connects>
<connect gate="G$1" pin="PA0" pad="3"/>
<connect gate="G$1" pin="PA1" pad="2"/>
<connect gate="G$1" pin="PA2" pad="19"/>
<connect gate="G$1" pin="PB0" pad="10"/>
<connect gate="G$1" pin="PB1" pad="11"/>
<connect gate="G$1" pin="PB2" pad="12"/>
<connect gate="G$1" pin="PB3" pad="13"/>
<connect gate="G$1" pin="PB4" pad="14"/>
<connect gate="G$1" pin="PB5" pad="15"/>
<connect gate="G$1" pin="PB6" pad="16"/>
<connect gate="G$1" pin="PB7" pad="17"/>
<connect gate="G$1" pin="PD0" pad="20"/>
<connect gate="G$1" pin="PD1" pad="1"/>
<connect gate="G$1" pin="PD2" pad="4"/>
<connect gate="G$1" pin="PD3" pad="5"/>
<connect gate="G$1" pin="PD4" pad="6"/>
<connect gate="G$1" pin="PD5" pad="7"/>
<connect gate="G$1" pin="PD6" pad="9"/>
<connect gate="PWR" pin="+" pad="18"/>
<connect gate="PWR" pin="-" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7212738/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-ISP" prefix="ISP">
<gates>
<gate name="G$1" symbol="GEIER-ISP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV6W64P254_2X3_762X508X838B">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RESET" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7212775/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-HDR4" prefix="HDR">
<gates>
<gate name="G$1" symbol="GEIER-HDR4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV4W64P254_1X4_1016X254X838B">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7207460/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-LM78XX" prefix="IC">
<gates>
<gate name="G$1" symbol="GEIER-LM78XX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO450P972X240-3">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7212818/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GEIER-Y" prefix="Y" uservalue="yes">
<gates>
<gate name="G$1" symbol="GEIER-Y" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XTAL1200X480X450">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7212909/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+12V" urn="urn:adsk.eagle:symbol:26931/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" urn="urn:adsk.eagle:component:26959/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="RobticArm" deviceset="GEIER-FRAME_A3" device=""/>
<part name="IC1" library="RobticArm" deviceset="GEIER-DRV8886AT" device="HTSSOP" package3d_urn="urn:adsk.eagle:package:7056855/1" value="DRV8886A"/>
<part name="C1" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="10nF"/>
<part name="C2" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="10nF"/>
<part name="C3" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="220nF"/>
<part name="C4" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="470nF"/>
<part name="C5" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="470nF"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+12V" device=""/>
<part name="R1" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="680"/>
<part name="LED1" library="RobticArm" deviceset="GEIER-LED" device="0805" package3d_urn="urn:adsk.eagle:package:7159533/1" value="GRN"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X1" library="RobticArm" deviceset="GEIER-X2" device="" package3d_urn="urn:adsk.eagle:package:9320/1"/>
<part name="D1" library="RobticArm" deviceset="GEIER-D" device="FRED" package3d_urn="urn:adsk.eagle:package:7472987/1" value="1N4007"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C6" library="RobticArm" deviceset="GEIER-C_POL" device="6.6MM" package3d_urn="urn:adsk.eagle:package:7207779/1" value="100µF"/>
<part name="C7" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="22nF"/>
<part name="R2" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="10k"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R3" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="10k"/>
<part name="MCU1" library="RobticArm" deviceset="GEIER-ATTINY23" device="QFN" package3d_urn="urn:adsk.eagle:package:7212738/1" value="ATTINY23"/>
<part name="C8" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="ISP1" library="RobticArm" deviceset="GEIER-ISP" device="" package3d_urn="urn:adsk.eagle:package:7212775/1"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="RFUART" library="RobticArm" deviceset="GEIER-HDR4" device="" package3d_urn="urn:adsk.eagle:package:7207460/1"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC2" library="RobticArm" deviceset="GEIER-LM78XX" device="" package3d_urn="urn:adsk.eagle:package:7212818/1" value="7805"/>
<part name="LED2" library="RobticArm" deviceset="GEIER-LED" device="0805" package3d_urn="urn:adsk.eagle:package:7159533/1" value="GRN"/>
<part name="R4" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="270"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C9" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="330nF"/>
<part name="C10" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="100nF"/>
<part name="Y1" library="RobticArm" deviceset="GEIER-Y" device="" package3d_urn="urn:adsk.eagle:package:7212909/1" value="20MHz"/>
<part name="C11" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="10pF"/>
<part name="C12" library="RobticArm" deviceset="GEIER-C" device="0805" package3d_urn="urn:adsk.eagle:package:7158867/1" value="10pF"/>
<part name="R5" library="RobticArm" deviceset="GEIER-R" device="0805" package3d_urn="urn:adsk.eagle:package:7158853/1" value="1M"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="STEPPER" library="RobticArm" deviceset="GEIER-HDR4" device="" package3d_urn="urn:adsk.eagle:package:7207460/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="343.535" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="SHEET" x="356.87" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="343.535" y="10.16" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="IC1" gate="G$1" x="200.66" y="154.94" smashed="yes">
<attribute name="NAME" x="193.04" y="167.64" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="193.04" y="137.16" size="0.8128" layer="96"/>
</instance>
<instance part="IC1" gate="VREG" x="152.4" y="231.14" smashed="yes" rot="R180">
<attribute name="NAME" x="161.29" y="226.06" size="0.8128" layer="95" rot="R180"/>
</instance>
<instance part="IC1" gate="PWR" x="132.08" y="236.22" smashed="yes">
<attribute name="NAME" x="130.4925" y="235.2675" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="C1" gate="G$1" x="124.46" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="121.92" y="236.22" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="128.27" y="236.22" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="116.84" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="114.3" y="236.22" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="120.65" y="236.22" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="147.32" y="241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="144.78" y="241.3" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="151.13" y="241.3" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="147.32" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="144.78" y="220.98" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="151.13" y="220.98" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="139.7" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="220.98" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="143.51" y="220.98" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="139.7" y="210.82" smashed="yes">
<attribute name="VALUE" x="137.16" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="147.32" y="210.82" smashed="yes">
<attribute name="VALUE" x="144.78" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="172.72" y="254" smashed="yes">
<attribute name="VALUE" x="170.18" y="248.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="172.72" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="171.45" y="236.22" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="174.9425" y="236.22" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="LED1" gate="G$1" x="172.72" y="226.06" smashed="yes" rot="R270">
<attribute name="NAME" x="174.4726" y="228.6" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="170.0276" y="228.6" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="GND3" gate="1" x="172.72" y="210.82" smashed="yes">
<attribute name="VALUE" x="170.18" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="132.08" y="210.82" smashed="yes">
<attribute name="VALUE" x="129.54" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="63.5" y="246.38" smashed="yes">
<attribute name="NAME" x="60.96" y="250.825" size="0.8128" layer="95" font="vector"/>
</instance>
<instance part="D1" gate="G$1" x="73.66" y="248.92" smashed="yes">
<attribute name="NAME" x="71.12" y="250.6726" size="0.8128" layer="95" font="vector"/>
<attribute name="VALUE" x="71.12" y="246.2276" size="0.8128" layer="96" font="vector"/>
</instance>
<instance part="GND5" gate="1" x="71.12" y="210.82" smashed="yes">
<attribute name="VALUE" x="68.58" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="109.22" y="236.22" smashed="yes" rot="R270">
<attribute name="NAME" x="111.76" y="237.49" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="106.045" y="237.49" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="C7" gate="G$1" x="218.44" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="220.98" y="147.32" size="0.8128" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="218.44" y="148.59" size="0.8128" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="R2" gate="G$1" x="172.72" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="171.45" y="147.32" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="174.9425" y="147.32" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="172.72" y="139.7" smashed="yes">
<attribute name="VALUE" x="170.18" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="231.14" y="167.64" smashed="yes">
<attribute name="VALUE" x="228.6" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="231.14" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="229.87" y="154.94" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="233.3625" y="154.94" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="MCU1" gate="G$1" x="142.24" y="152.4" smashed="yes">
<attribute name="NAME" x="134.62" y="165.1" size="0.8128" layer="95"/>
<attribute name="VALUE" x="134.62" y="139.7" size="0.8128" layer="96"/>
</instance>
<instance part="MCU1" gate="PWR" x="220.98" y="236.22" smashed="yes">
<attribute name="NAME" x="219.3925" y="235.2675" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="213.36" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="210.82" y="236.22" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="217.17" y="236.22" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND7" gate="1" x="220.98" y="210.82" smashed="yes">
<attribute name="VALUE" x="218.44" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="ISP1" gate="G$1" x="68.58" y="152.4" smashed="yes" rot="MR0">
<attribute name="NAME" x="71.12" y="162.56" size="0.8128" layer="95" rot="MR0"/>
</instance>
<instance part="GND8" gate="1" x="76.2" y="139.7" smashed="yes">
<attribute name="VALUE" x="73.66" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="1" x="76.2" y="167.64" smashed="yes">
<attribute name="VALUE" x="73.66" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="1" x="231.14" y="254" smashed="yes">
<attribute name="VALUE" x="228.6" y="248.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="RFUART" gate="G$1" x="88.9" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="87.63" y="160.655" size="0.8128" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="P+5" gate="1" x="96.52" y="154.94" smashed="yes">
<attribute name="VALUE" x="93.98" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND9" gate="1" x="93.98" y="144.78" smashed="yes">
<attribute name="VALUE" x="91.44" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="IC2" gate="G$1" x="193.04" y="248.92" smashed="yes">
<attribute name="NAME" x="185.42" y="254" size="0.8128" layer="95"/>
<attribute name="VALUE" x="195.58" y="238.76" size="0.8128" layer="96"/>
</instance>
<instance part="LED2" gate="G$1" x="231.14" y="226.06" smashed="yes" rot="R270">
<attribute name="NAME" x="232.8926" y="228.6" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="228.4476" y="228.6" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="R4" gate="G$1" x="231.14" y="238.76" smashed="yes" rot="R270">
<attribute name="NAME" x="232.41" y="241.3" size="0.8128" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="228.9175" y="241.3" size="0.8128" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="231.14" y="210.82" smashed="yes">
<attribute name="VALUE" x="228.6" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="193.04" y="210.82" smashed="yes">
<attribute name="VALUE" x="190.5" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="180.34" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="177.8" y="236.22" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="184.15" y="236.22" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="205.74" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="203.2" y="236.22" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="209.55" y="236.22" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="Y1" gate="G$1" x="109.22" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="106.68" y="143.51" size="0.8128" layer="95" rot="R90"/>
<attribute name="VALUE" x="112.395" y="143.51" size="0.8128" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="101.6" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="99.06" y="129.54" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="105.41" y="129.54" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C12" gate="G$1" x="116.84" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="114.3" y="129.54" size="0.8128" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="120.65" y="129.54" size="0.8128" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R5" gate="G$1" x="109.22" y="137.16" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="135.89" size="0.8128" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="111.76" y="139.3825" size="0.8128" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND12" gate="1" x="101.6" y="119.38" smashed="yes">
<attribute name="VALUE" x="99.06" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="116.84" y="119.38" smashed="yes">
<attribute name="VALUE" x="114.3" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="STEPPER" gate="G$1" x="256.54" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="257.81" y="169.545" size="0.8128" layer="95" font="vector" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+12V" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$2"/>
<pinref part="P+1" gate="1" pin="+12V"/>
<wire x1="172.72" y1="243.84" x2="172.72" y2="248.92" width="0.1524" layer="91"/>
<wire x1="172.72" y1="248.92" x2="172.72" y2="251.46" width="0.1524" layer="91"/>
<junction x="172.72" y="248.92"/>
<pinref part="C3" gate="G$1" pin="P$2"/>
<wire x1="147.32" y1="248.92" x2="172.72" y2="248.92" width="0.1524" layer="91"/>
<wire x1="147.32" y1="246.38" x2="147.32" y2="248.92" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$2"/>
<pinref part="IC1" gate="PWR" pin="+"/>
<wire x1="132.08" y1="243.84" x2="132.08" y2="246.38" width="0.1524" layer="91"/>
<wire x1="132.08" y1="246.38" x2="124.46" y2="246.38" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="P$2"/>
<wire x1="124.46" y1="241.3" x2="124.46" y2="246.38" width="0.1524" layer="91"/>
<junction x="124.46" y="246.38"/>
<wire x1="124.46" y1="246.38" x2="116.84" y2="246.38" width="0.1524" layer="91"/>
<wire x1="116.84" y1="246.38" x2="116.84" y2="241.3" width="0.1524" layer="91"/>
<wire x1="132.08" y1="246.38" x2="132.08" y2="248.92" width="0.1524" layer="91"/>
<junction x="132.08" y="246.38"/>
<wire x1="132.08" y1="248.92" x2="147.32" y2="248.92" width="0.1524" layer="91"/>
<junction x="147.32" y="248.92"/>
<pinref part="D1" gate="G$1" pin="C"/>
<junction x="132.08" y="248.92"/>
<wire x1="76.2" y1="248.92" x2="132.08" y2="248.92" width="0.1524" layer="91"/>
<wire x1="116.84" y1="246.38" x2="109.22" y2="246.38" width="0.1524" layer="91"/>
<junction x="116.84" y="246.38"/>
<pinref part="C6" gate="G$1" pin="A"/>
<wire x1="109.22" y1="246.38" x2="109.22" y2="241.3" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="IN"/>
<wire x1="182.88" y1="248.92" x2="180.34" y2="248.92" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="P$2"/>
<wire x1="180.34" y1="248.92" x2="172.72" y2="248.92" width="0.1524" layer="91"/>
<wire x1="180.34" y1="248.92" x2="180.34" y2="241.3" width="0.1524" layer="91"/>
<junction x="180.34" y="248.92"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="P$1"/>
<wire x1="147.32" y1="213.36" x2="147.32" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="P$1"/>
<wire x1="139.7" y1="213.36" x2="139.7" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="172.72" y1="223.52" x2="172.72" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="P$1"/>
<wire x1="124.46" y1="231.14" x2="124.46" y2="226.06" width="0.1524" layer="91"/>
<wire x1="124.46" y1="226.06" x2="132.08" y2="226.06" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="-"/>
<wire x1="132.08" y1="226.06" x2="132.08" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="P$1"/>
<wire x1="116.84" y1="231.14" x2="116.84" y2="226.06" width="0.1524" layer="91"/>
<wire x1="116.84" y1="226.06" x2="124.46" y2="226.06" width="0.1524" layer="91"/>
<junction x="124.46" y="226.06"/>
<wire x1="132.08" y1="226.06" x2="132.08" y2="213.36" width="0.1524" layer="91"/>
<junction x="132.08" y="226.06"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="C"/>
<wire x1="109.22" y1="231.14" x2="109.22" y2="226.06" width="0.1524" layer="91"/>
<wire x1="109.22" y1="226.06" x2="116.84" y2="226.06" width="0.1524" layer="91"/>
<junction x="116.84" y="226.06"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="-"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="68.58" y1="243.84" x2="71.12" y2="243.84" width="0.1524" layer="91"/>
<wire x1="71.12" y1="243.84" x2="71.12" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="P$1"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="172.72" y1="142.24" x2="172.72" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="P$1"/>
<wire x1="213.36" y1="231.14" x2="213.36" y2="226.06" width="0.1524" layer="91"/>
<wire x1="213.36" y1="226.06" x2="220.98" y2="226.06" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="220.98" y1="226.06" x2="220.98" y2="213.36" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PWR" pin="-"/>
<wire x1="220.98" y1="228.6" x2="220.98" y2="226.06" width="0.1524" layer="91"/>
<junction x="220.98" y="226.06"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="76.2" y1="142.24" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<pinref part="ISP1" gate="G$1" pin="GND"/>
<wire x1="76.2" y1="144.78" x2="73.66" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RFUART" gate="G$1" pin="P$3"/>
<wire x1="91.44" y1="152.4" x2="93.98" y2="152.4" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="93.98" y1="152.4" x2="93.98" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="231.14" y1="213.36" x2="231.14" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="193.04" y1="238.76" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="P$1"/>
<wire x1="193.04" y1="226.06" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<wire x1="180.34" y1="231.14" x2="180.34" y2="226.06" width="0.1524" layer="91"/>
<wire x1="180.34" y1="226.06" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
<junction x="193.04" y="226.06"/>
<pinref part="C10" gate="G$1" pin="P$1"/>
<wire x1="205.74" y1="231.14" x2="205.74" y2="226.06" width="0.1524" layer="91"/>
<wire x1="205.74" y1="226.06" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="P$1"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="116.84" y1="124.46" x2="116.84" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="P$1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="101.6" y1="124.46" x2="101.6" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="P$1"/>
<pinref part="IC1" gate="VREG" pin="VCP"/>
<wire x1="147.32" y1="236.22" x2="147.32" y2="233.68" width="0.1524" layer="91"/>
<wire x1="147.32" y1="233.68" x2="149.86" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="VREG" pin="AVDD"/>
<pinref part="C5" gate="G$1" pin="P$2"/>
<wire x1="149.86" y1="231.14" x2="139.7" y2="231.14" width="0.1524" layer="91"/>
<wire x1="139.7" y1="231.14" x2="139.7" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="P$2"/>
<pinref part="IC1" gate="VREG" pin="DVDD"/>
<wire x1="147.32" y1="226.06" x2="147.32" y2="228.6" width="0.1524" layer="91"/>
<wire x1="147.32" y1="228.6" x2="149.86" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R1" gate="G$1" pin="P$1"/>
<wire x1="172.72" y1="228.6" x2="172.72" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="+"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="68.58" y1="248.92" x2="71.12" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CPH"/>
<pinref part="C7" gate="G$1" pin="P$2"/>
<wire x1="210.82" y1="144.78" x2="213.36" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="P$1"/>
<wire x1="223.52" y1="144.78" x2="226.06" y2="144.78" width="0.1524" layer="91"/>
<wire x1="226.06" y1="144.78" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="CPL"/>
<wire x1="226.06" y1="142.24" x2="210.82" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="P$2"/>
<wire x1="172.72" y1="154.94" x2="172.72" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="REF"/>
<wire x1="172.72" y1="157.48" x2="190.5" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="P$2"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="231.14" y1="162.56" x2="231.14" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ISP1" gate="G$1" pin="VCC"/>
<wire x1="73.66" y1="157.48" x2="76.2" y2="157.48" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="76.2" y1="157.48" x2="76.2" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RFUART" gate="G$1" pin="P$4"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="91.44" y1="149.86" x2="96.52" y2="149.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="149.86" x2="96.52" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="R4" gate="G$1" pin="P$1"/>
<wire x1="231.14" y1="251.46" x2="231.14" y2="248.92" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="PWR" pin="+"/>
<wire x1="231.14" y1="248.92" x2="231.14" y2="243.84" width="0.1524" layer="91"/>
<wire x1="220.98" y1="248.92" x2="220.98" y2="246.38" width="0.1524" layer="91"/>
<wire x1="220.98" y1="246.38" x2="220.98" y2="243.84" width="0.1524" layer="91"/>
<wire x1="231.14" y1="248.92" x2="220.98" y2="248.92" width="0.1524" layer="91"/>
<junction x="231.14" y="248.92"/>
<junction x="220.98" y="248.92"/>
<pinref part="IC2" gate="G$1" pin="OUT"/>
<wire x1="203.2" y1="248.92" x2="205.74" y2="248.92" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="P$2"/>
<wire x1="205.74" y1="248.92" x2="220.98" y2="248.92" width="0.1524" layer="91"/>
<wire x1="213.36" y1="241.3" x2="213.36" y2="246.38" width="0.1524" layer="91"/>
<wire x1="213.36" y1="246.38" x2="220.98" y2="246.38" width="0.1524" layer="91"/>
<junction x="220.98" y="246.38"/>
<pinref part="C10" gate="G$1" pin="P$2"/>
<wire x1="205.74" y1="241.3" x2="205.74" y2="248.92" width="0.1524" layer="91"/>
<junction x="205.74" y="248.92"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="231.14" y1="152.4" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="!FLT"/>
<wire x1="231.14" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PD2"/>
<wire x1="132.08" y1="149.86" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<wire x1="124.46" y1="149.86" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
<wire x1="124.46" y1="121.92" x2="231.14" y2="121.92" width="0.1524" layer="91"/>
<wire x1="231.14" y1="121.92" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<junction x="231.14" y="149.86"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="ISP1" gate="G$1" pin="RESET"/>
<wire x1="73.66" y1="160.02" x2="86.36" y2="160.02" width="0.1524" layer="91"/>
<wire x1="86.36" y1="160.02" x2="86.36" y2="162.56" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PA2"/>
<wire x1="86.36" y1="162.56" x2="132.08" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="ISP1" gate="G$1" pin="SCK"/>
<wire x1="73.66" y1="154.94" x2="83.82" y2="154.94" width="0.1524" layer="91"/>
<wire x1="83.82" y1="154.94" x2="83.82" y2="167.64" width="0.1524" layer="91"/>
<wire x1="83.82" y1="167.64" x2="154.94" y2="167.64" width="0.1524" layer="91"/>
<wire x1="154.94" y1="167.64" x2="154.94" y2="162.56" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PB7"/>
<wire x1="154.94" y1="162.56" x2="152.4" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="ISP1" gate="G$1" pin="MISO"/>
<wire x1="73.66" y1="149.86" x2="81.28" y2="149.86" width="0.1524" layer="91"/>
<wire x1="81.28" y1="149.86" x2="81.28" y2="170.18" width="0.1524" layer="91"/>
<wire x1="81.28" y1="170.18" x2="157.48" y2="170.18" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PB6"/>
<wire x1="157.48" y1="170.18" x2="157.48" y2="160.02" width="0.1524" layer="91"/>
<wire x1="157.48" y1="160.02" x2="152.4" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="PB5"/>
<wire x1="152.4" y1="157.48" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="160.02" y1="172.72" x2="78.74" y2="172.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="172.72" x2="78.74" y2="147.32" width="0.1524" layer="91"/>
<pinref part="ISP1" gate="G$1" pin="MOSI"/>
<wire x1="78.74" y1="147.32" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="PB4"/>
<wire x1="152.4" y1="154.94" x2="162.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="162.56" y1="154.94" x2="162.56" y2="165.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="STP"/>
<wire x1="162.56" y1="165.1" x2="190.5" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DIR"/>
<wire x1="190.5" y1="162.56" x2="165.1" y2="162.56" width="0.1524" layer="91"/>
<wire x1="165.1" y1="162.56" x2="165.1" y2="152.4" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PB3"/>
<wire x1="165.1" y1="152.4" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="PB2"/>
<wire x1="152.4" y1="149.86" x2="167.64" y2="149.86" width="0.1524" layer="91"/>
<wire x1="167.64" y1="149.86" x2="167.64" y2="160.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="ENB"/>
<wire x1="167.64" y1="160.02" x2="190.5" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TRQ"/>
<wire x1="190.5" y1="139.7" x2="187.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="187.96" y1="139.7" x2="187.96" y2="124.46" width="0.1524" layer="91"/>
<wire x1="187.96" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
<wire x1="127" y1="124.46" x2="127" y2="147.32" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PD3"/>
<wire x1="127" y1="147.32" x2="132.08" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="M0"/>
<wire x1="190.5" y1="152.4" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
<wire x1="177.8" y1="152.4" x2="177.8" y2="134.62" width="0.1524" layer="91"/>
<wire x1="177.8" y1="134.62" x2="167.64" y2="134.62" width="0.1524" layer="91"/>
<wire x1="167.64" y1="134.62" x2="167.64" y2="147.32" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PB1"/>
<wire x1="167.64" y1="147.32" x2="152.4" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="PB0"/>
<wire x1="152.4" y1="144.78" x2="165.1" y2="144.78" width="0.1524" layer="91"/>
<wire x1="165.1" y1="144.78" x2="165.1" y2="132.08" width="0.1524" layer="91"/>
<wire x1="165.1" y1="132.08" x2="180.34" y2="132.08" width="0.1524" layer="91"/>
<wire x1="180.34" y1="132.08" x2="180.34" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="M1"/>
<wire x1="180.34" y1="149.86" x2="190.5" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="DCY"/>
<wire x1="190.5" y1="147.32" x2="182.88" y2="147.32" width="0.1524" layer="91"/>
<wire x1="182.88" y1="147.32" x2="182.88" y2="129.54" width="0.1524" layer="91"/>
<wire x1="182.88" y1="129.54" x2="162.56" y2="129.54" width="0.1524" layer="91"/>
<wire x1="162.56" y1="129.54" x2="162.56" y2="142.24" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="PD6"/>
<wire x1="162.56" y1="142.24" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="PD4"/>
<wire x1="132.08" y1="144.78" x2="129.54" y2="144.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="144.78" x2="129.54" y2="127" width="0.1524" layer="91"/>
<wire x1="129.54" y1="127" x2="185.42" y2="127" width="0.1524" layer="91"/>
<wire x1="185.42" y1="127" x2="185.42" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="!SLP"/>
<wire x1="185.42" y1="142.24" x2="190.5" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="PD0"/>
<pinref part="RFUART" gate="G$1" pin="P$1"/>
<wire x1="132.08" y1="160.02" x2="91.44" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="RFUART" gate="G$1" pin="P$2"/>
<pinref part="MCU1" gate="G$1" pin="PD1"/>
<wire x1="91.44" y1="157.48" x2="132.08" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="P$2"/>
<pinref part="LED2" gate="G$1" pin="A"/>
<wire x1="231.14" y1="233.68" x2="231.14" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="142.24" x2="101.6" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="P$2"/>
<wire x1="101.6" y1="142.24" x2="101.6" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="P$2"/>
<wire x1="101.6" y1="137.16" x2="101.6" y2="134.62" width="0.1524" layer="91"/>
<wire x1="104.14" y1="137.16" x2="101.6" y2="137.16" width="0.1524" layer="91"/>
<junction x="101.6" y="137.16"/>
<wire x1="101.6" y1="142.24" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<junction x="101.6" y="142.24"/>
<pinref part="MCU1" gate="G$1" pin="PA1"/>
<wire x1="101.6" y1="154.94" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="114.3" y1="142.24" x2="116.84" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="142.24" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="P$1"/>
<wire x1="116.84" y1="137.16" x2="116.84" y2="134.62" width="0.1524" layer="91"/>
<wire x1="114.3" y1="137.16" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<junction x="116.84" y="137.16"/>
<pinref part="MCU1" gate="G$1" pin="PA0"/>
<wire x1="132.08" y1="152.4" x2="116.84" y2="152.4" width="0.1524" layer="91"/>
<wire x1="116.84" y1="152.4" x2="116.84" y2="142.24" width="0.1524" layer="91"/>
<junction x="116.84" y="142.24"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="Q1+"/>
<wire x1="210.82" y1="165.1" x2="215.9" y2="165.1" width="0.1524" layer="91"/>
<wire x1="215.9" y1="165.1" x2="215.9" y2="180.34" width="0.1524" layer="91"/>
<pinref part="STEPPER" gate="G$1" pin="P$4"/>
<wire x1="215.9" y1="180.34" x2="254" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="STEPPER" gate="G$1" pin="P$3"/>
<wire x1="254" y1="177.8" x2="218.44" y2="177.8" width="0.1524" layer="91"/>
<wire x1="218.44" y1="177.8" x2="218.44" y2="162.56" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="Q1-"/>
<wire x1="218.44" y1="162.56" x2="210.82" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="Q2+"/>
<wire x1="210.82" y1="157.48" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<wire x1="220.98" y1="157.48" x2="220.98" y2="172.72" width="0.1524" layer="91"/>
<pinref part="STEPPER" gate="G$1" pin="P$2"/>
<wire x1="220.98" y1="172.72" x2="254" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="Q2-"/>
<wire x1="210.82" y1="154.94" x2="223.52" y2="154.94" width="0.1524" layer="91"/>
<wire x1="223.52" y1="154.94" x2="223.52" y2="170.18" width="0.1524" layer="91"/>
<pinref part="STEPPER" gate="G$1" pin="P$1"/>
<wire x1="223.52" y1="170.18" x2="254" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
