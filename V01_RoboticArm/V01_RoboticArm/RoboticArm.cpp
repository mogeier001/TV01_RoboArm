#include "RoboticArm.h"

RoboticArm::RoboticArm()
{
	Servo::SelectDriver(1, 0x47, 300);
	_s0 = Servo(0, START_ANGLE, MIN_TIME, MAX_TIME, MAX_ANGLE);
	_s1 = Servo(1, START_ANGLE, MIN_TIME, MAX_TIME, MAX_ANGLE);
	_s2 = Servo(2, START_ANGLE, MIN_TIME, MAX_TIME, MAX_ANGLE);
}

void RoboticArm::SetKoord(double x, double y)
{
	if (y >= 0)
	{
		double vec = sqrt(x*x + y * y);
		double phiV = std::atan(y / x) * 180 / M_PI;
		double phiS0 = std::acos(vec / (2 * ARM_LENGTH)) * 180 / M_PI;
		if (x <=0)
		{
			_s0.SetAngle(225 - (phiS0 + phiV));
			_s1.SetAngle((180 - phiS0 * 2) - 45);
		}
		else
		{
			_s0.SetAngle(45 + phiS0 + phiV);
			_s1.SetAngle((180 - phiS0 * 2) - 45);
		}		
	}
	//std::cout << std::endl << "PHI S0 " << phiS0 << std::endl;
	//std::cout << "PHI S1 " << (180 - phiS0 * 2) - 45 << std::endl << std::endl;
}